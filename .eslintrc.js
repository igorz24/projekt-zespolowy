module.exports = {
  env: {
    es6: true,
    node: true,
    jest: true
  },
  extends: ["airbnb-base", "prettier"],
  parserOptions: {
    ecmaVersion: 2018
  },
  rules: {
    "no-console": "warn",
    "promise/no-native": "off",
    "no-underscore-dangle": [2, {"allow": ["_id"]}],
    "class-methods-use-this": "off",
    "no-param-reassign": 0,
    "no-undef": 0,
    "no-useless-constructor": "off",
    "func-names": ["error", "as-needed"],
    "no-shadow": "off",
    "import/extensions": "off",
    camelcase: 0,
    indent: ["off", 4],
    "no-unused-vars": ["warn", {args: "none"}],
    "prefer-arrow-callback": ["error", {"allowNamedFunctions": true, "allowUnboundThis": false}],
    "prefer-destructuring": [
      "error",
      {
        VariableDeclarator: {
          array: true,
          object: false
        },
        AssignmentExpression: {
          array: true,
          object: false
        }
      },
      {
        enforceForRenamedProperties: true
      }
    ]
  },
  parser: "@typescript-eslint/parser",
  settings: {
    "import/resolver": {
      node: {
        extensions: [".js", ".jsx", ".ts", ".tsx"]
      }
    }
  },
  overrides: [
    {
      files: ["*.test.ts", "*.spec.ts"],
      rules: {
        "no-unused-expressions": "off"
      }
    }
  ]
};
