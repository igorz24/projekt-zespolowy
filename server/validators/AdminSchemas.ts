import Joi from 'joi';
import {UserRoles} from "../enums/UserRole";

const schemas = {

	assignToProject: Joi.object().keys({
		userId: Joi.number().required(),
		projectId: Joi.number().required()
	}),

	changeRole: Joi.object().keys({
		userId: Joi.number().required(),
		newRole: Joi.string().valid(UserRoles).required()
	}),

	resetUsersPassword: Joi.object().keys({
		userId: Joi.number().required()
	}),

	ban: Joi.object().keys({
		userId: Joi.number().required(),
	})

};
export default schemas;