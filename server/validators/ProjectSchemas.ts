import Joi from 'joi';

const schemas = {

	create: Joi.object().keys({
		name: Joi.string().required(),
		description: Joi.string().required()
	}),

	search: Joi.object().keys({
		textSearch: Joi.string().optional()
	}),

	projectId: Joi.object().keys({
		projectId: Joi.number().greater(0).required()
	}),

	update: Joi.object().keys({
		projectId: Joi.number().greater(0).required(),
		description: Joi.string().optional(),
		name: Joi.string().optional()
	}),

	getDevelopers: Joi.object().keys({
		projectId: Joi.number().greater(0).required()	}),
	};
export default schemas;