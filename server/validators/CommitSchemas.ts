import Joi from 'joi';

const schemas = {

	create: Joi.object().keys({
		workTime: Joi.number().greater(0).required(),
		taskId: Joi.number().greater(0).required(),
		description: Joi.string().required()
	}),

	commitId: Joi.object().keys({
		commitId: Joi.number().greater(0).required()
	}),

	search: Joi.object().keys({
		status: Joi.string().optional(),
		textSearch: Joi.string().optional()
	}),

	update: Joi.object().keys({
		commitId: Joi.number().greater(1).required(),
		workTime: Joi.number().greater(0).optional(),
		description: Joi.string().optional()
	}),

};
export default schemas;