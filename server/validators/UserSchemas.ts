import Joi from 'joi';

const schemas = {

  signUp: Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    firstName: Joi.string().optional(),
    lastName: Joi.string().optional()
  }),

  usersSearch: Joi.object().keys({
    role: Joi.string().optional(),
    textSearch: Joi.string().optional()
  }),

  signIn: Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required()
  }),

  editProfile: Joi.object().keys({
    _id: Joi.string().forbidden(),
    id: Joi.string().forbidden(),
    email: Joi.string().email().forbidden(),
    firstName: Joi.string().optional(),
    lastName: Joi.string().optional(),
    newRole: Joi.string().forbidden(),
    password: Joi.string().optional().min(8)
  }).min(1),

  sendResetLink: Joi.object().keys({
    email: Joi.string().email().required(),
  }),

  resetPassword: Joi.object().keys({
    resetCode: Joi.string().required(),
    password: Joi.string().required().min(1),
  }),

};
export default schemas;