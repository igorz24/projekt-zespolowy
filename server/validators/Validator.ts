import Joi, { ObjectSchema } from "joi";

export default class Validator {
  static validateBody = (schema: ObjectSchema) => {
    return (req: any, res: any, next: any) => {
      const result = Joi.validate(req.body, schema);
      if(result.error)
        return res.status(400).json(result.error);

      if(!req.value)
        req.value = {};

      req.value.body = result.value;
      return next();
    }
  }
};