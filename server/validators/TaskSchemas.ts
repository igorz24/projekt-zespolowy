import Joi from 'joi';

const schemas = {

	create: Joi.object().keys({
		estimationTime: Joi.number().greater(0).required(),
		projectId: Joi.number().greater(0).required(),
		description: Joi.string().required()
	}),

	search: Joi.object().keys({
		textSearch: Joi.string().optional()
	}),


	taskId: Joi.object().keys({
		taskId: Joi.number().greater(0).required()
	}),

	update: Joi.object().keys({
		taskId: Joi.number().greater(0).required(),
		estimationTime: Joi.number().greater(0).optional(),
		description: Joi.string().optional()
	}),

};
export default schemas;