// Suppress global exporting
// Added to suppress default User export which is causing conflicts when importing User in other files
import {Model, model, Schema} from "mongoose";
import {isAfter} from 'date-fns'
// @ts-ignore
import timestamp from 'mongoose-timestamp';
import bcrypt from 'bcrypt';
import autoIncrement from 'mongoose-auto-increment';
import IUser from "../types/IUser";
import UserRole, {roleValues} from "../enums/UserRole";


// Create a schema
const UserSchema = new Schema({
	// _id not explicitly written - main mongo index by default

	password: {
		type: String,
		required: true,
	},
	email: {
		type: String,
		required: true,
		trim: true,
		unique: true
	},
	firstName: {
		type: String,
		required: true,
	},
	lastName: {
		type: String,
		required: true,
	},
	lastJwtId: {
		type: String,
		required: false
	},
	role: {
		type: UserRole,
		required: true,
		default: UserRole.USER
	}
});

UserSchema.index({firstName: 'text', lastName: 'text'});

// WARN use save() method only for creating new users, otherwise the password will be changed!
UserSchema.pre('save', async function onSave(next) {
	try {
		// Generate salt
		const salt = await bcrypt.genSalt(10);
		const hash = await bcrypt.hash(this.get('password'), salt);
		this.set("password", hash);
		next();
	} catch (error) {
		next(error);
	}
});

UserSchema.methods.isValidPassword = async function isValidPassword(plainPassword: String): Promise<boolean> {
	try {
		return await bcrypt.compare(plainPassword, this.password)
	} catch (e) {
		throw new Error(e);
	}
};

UserSchema.methods.isValidToken = function isValidToken(jwtId: String): boolean {
	try {
		return jwtId === this.lastJwtId
	} catch (e) {
		throw new Error(e);
	}
};

UserSchema.methods.isSubscriptionActive = function isSubscriptionActive(): boolean {
	try {
		const thisDate: Date = this.subscriptionUntil;
		return (thisDate && isAfter(thisDate, new Date()));
	} catch (e) {
		throw new Error(e);
	}
};

UserSchema.methods.hasAccess = function hasAccess(roleToCheckAgainst: UserRole): boolean {
	try {
		const thisRole: UserRole = this.role;
		const thisRoleValue: number = roleValues[thisRole];
		const roleToCheckAgainstValue: number = roleValues[roleToCheckAgainst];
		return (thisRoleValue >= roleToCheckAgainstValue);
	} catch (e) {
		throw new Error(e);
	}
};

// add automatic timestamp values on create/update actions
UserSchema.plugin(timestamp);
UserSchema.plugin(autoIncrement.plugin, {
	model: 'user',
	startAt: 1,
});

// Export model
const User: Model<IUser> = model<IUser>('user', UserSchema);
export default User;

