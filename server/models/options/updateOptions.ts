// adding this option to mongoose findOneAndUpdate method makes it return the updated value instead of the original one
const updateOptions = {
  new: true
};

export default updateOptions;