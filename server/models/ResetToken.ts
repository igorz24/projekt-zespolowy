import {Model, model, Schema} from "mongoose";
import {isBefore, addHours} from 'date-fns'
// @ts-ignore
import timestamp from 'mongoose-timestamp';
import IResetToken from "../types/IResetToken";

// Create a schema
const ResetTokenSchema = new Schema({
  // _id not explicitly written - main mongo index by default

  token: {
    unique: true,
    required: true,
    type: String
  },
  email: {
    type: String,
    required: true
  }
});

ResetTokenSchema.methods.isValid = function isValid(plainPassword: String) {
  try {
    const updatedAt: Date = this.updatedAt;
    // Check if token has expired (If now < updatedAt + 1 hour)
    return (updatedAt && isBefore(new Date(), addHours(updatedAt, 1)))
  } catch (e) {
    throw new Error(e);
  }
};

// add automatic timestamp values on create/update actions
ResetTokenSchema.plugin(timestamp);

// Export model
const ResetToken: Model<IResetToken> = model<IResetToken>('resetTokens', ResetTokenSchema);
export default ResetToken;