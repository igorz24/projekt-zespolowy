import {Model, model, Schema} from "mongoose";
import autoIncrement from "mongoose-auto-increment";
import IProject from "../types/IProject";

// Create a schema
const ProjectSchema = new Schema({
	// _id not explicitly written - main mongo index by default

	name: {
		type: String,
		required: true
	},
	manager: {
		id: Number,
		firstName: String,
		lastName: String
	},
	developers: [{
		id: Number,
		firstName: String,
		lastName: String
	}],
	description: {
		type: String,
		required: true
	}
});

ProjectSchema.index({
	name: 'text',
	'manager.firstName': 'text',
	'manager.lastName': 'text',
});


ProjectSchema.plugin(autoIncrement.plugin, {
	model: 'projects',
	startAt: 1,
});

// Export model
const Project: Model<IProject> = model<IProject>('projects', ProjectSchema);
export default Project;