import {Model, model, Schema} from "mongoose";
import autoIncrement from "mongoose-auto-increment";
import ICommit from "../types/ICommit";
import UserRole from "../enums/UserRole";
import CommitStatus from "../enums/CommitStatus";

// Create a schema
const CommitSchema = new Schema({
	// _id not explicitly written - main mongo index by default

	date: {
		required: true,
		type: Date,
		default: Date.now
	},
	workTime: {
		type: Number,  // minutes
		required: true
	},
	projectId: {
		type: Number,
		required: true
	},
	taskId: {
		type: Number,
		required: true
	},
	authorId: {
		type: Number,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	status: {
		type: CommitStatus,
		required: true,
		default: CommitStatus.WAITING_FOR_APPROVAL
	}
});

CommitSchema.index({
	description: 'text'
});


CommitSchema.plugin(autoIncrement.plugin, {
	model: 'commits',
	startAt: 1,
});

// Export model
const Commit: Model<ICommit> = model<ICommit>('commits', CommitSchema);
export default Commit;