import {Model, model, Schema} from "mongoose";
import autoIncrement from "mongoose-auto-increment";
import ITask from "../types/ITask";

// Create a schema
const TaskSchema = new Schema({
	// _id not explicitly written - main mongo index by default

	estimationTime: {
		type: Number,  // minutes
		required: true
	},
	projectId: {
		type: Number,
		required: true
	},
	managerId: {
		type: Number,
		required: true
	},
	description: {
		type: String,
		required: true
	}
});

TaskSchema.index({
	description: 'text'
});


TaskSchema.plugin(autoIncrement.plugin, {
	model: 'tasks',
	startAt: 1,
});

// Export model
const Task: Model<ITask> = model<ITask>('tasks', TaskSchema);
export default Task;