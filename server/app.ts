// app.ts
import express from 'express';
// @ts-ignore
import cors from 'cors';
import passport from 'passport';
import dotenv from 'dotenv';
import bodyParser from "body-parser";
import morgan from 'morgan';
import UserController from './controllers/UserController';
import AdminController from './controllers/AdminController';
import CommitsController from './controllers/CommitsController';
import TasksController from './controllers/TasksController';
import ProjectsController from './controllers/ProjectsController';
import initializePassport from "./config/passport";
import {stream} from './config/logger';

// Passport setup
// Configures imported passport object with our strategies
initializePassport(passport);

dotenv.config({path: `env.${process.env.NODE_ENV}`});

const app = express();

// Middleware
app.use(morgan('combined', {stream}));
app.use(cors());
app.use(passport.initialize());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Routes
app.use('/users', UserController);
app.use('/admin', AdminController);
app.use('/commits', CommitsController);
app.use('/tasks', TasksController);
app.use('/projects', ProjectsController);
app.get('/', (req, res) => res.send("Status page :) \nEverything works just fine!"));

export default app;