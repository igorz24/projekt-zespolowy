import bcrypt from 'bcrypt';
import crypto from "crypto";
import JWT from 'jsonwebtoken';
import IUser from "../types/IUser";
import updateOptions from '../models/options/updateOptions'
import sendgridAdapter from '../adapters/SendgridAdapter';
import ResetToken from '../models/ResetToken';
import User from '../models/User';
import logger from '../config/logger';
import Joi from "joi";

const generateHash = async (base: String) => {
	const salt = await bcrypt.genSalt(5);
	return bcrypt.hash(base, salt);
};

const signToken = (user: IUser, jwtId: String) => {
	return JWT.sign({
			iss: process.env.ISSUER!,
			sub: user.id,
			role: user.role,
			// property added to prevent simultaneous logging-ins
			jti: jwtId
		},
		process.env.SECRET!,
		{expiresIn: +process.env.TOKEN_EXP_SECONDS!});
};

const getProfile = (user: IUser): object => {
	return {
		id: +user.id,
		email: user.email,
		role: user.role,
		firstName: user.firstName,
		lastName: user.lastName
	}
};

// return resetCode
export const getResetPasswordCode = async (email: string): Promise<string> => {
	const resetCode = crypto.randomBytes(32).toString("hex");
	await ResetToken.deleteMany({email});
	await new ResetToken({token: resetCode, email}).save();
	return resetCode;
};


export class UserService {
	signUp = async (req: any, res: any, next?: any) => {
		// Email and password
		const {email, password, firstName, lastName} = req.value.body;
		try {
			// Check for duplicate emails
			const foundUser = await User.findOne({email});

			if (foundUser)
				return res.status(403).send({error: 'Email is already in use'});

			// Create new user and generate`
			const newUser = new User({email, password, firstName, lastName});

			// Generate tokens and unique jwt id, to prevent multiple jwt tokens to be active at the same time
			const jwtId = await generateHash(email + Date.now());
			const token = signToken(newUser, jwtId);

			// Add current token to user model and save user
			newUser.lastJwtId = jwtId;
			await newUser.save();

			// Send welcome email
			await sendgridAdapter.sendWelcomeEmail(newUser.email);

			// Respond with the token
			return res.status(200).json({
				token,
				profile: getProfile(newUser)
			})
		} catch (e) {
			logger.error("UserService.SignUp ", e);
			return res.status(400).json({error: "Registration error"});
		}
	};

	signIn = async (req: any, res: any, next?: any) => {
		let token;
		try {

			// Generate new tokens
			const jwtId = await generateHash(req.user.email + Date.now());
			token = signToken(req.user, jwtId);

			// Replace token in db (invalidate old one)
			const filter = {email: req.user.email};
			const update = {
				lastJwtId: jwtId
			};
			const user = await User.findOneAndUpdate(filter, update, updateOptions);
			if (!user)
				return res.status(400).json({error: "Login error"});

			return res.status(200).json({
				token,
				profile: getProfile(user)
			});
		} catch (e) {
			logger.error("UserService.SignIn ", e);
			return res.status(400).json({error: "Login error"});
		}
	};

	secret = async (req: any, res: any, next?: any) => {
		return res.status(200).send({response: 'Access Granted'});
	};

	logout = async (req: any, res: any, next?: any) => {
		const filter = {email: req.user.email};
		const update = {
			lastJwtId: "inactive",
		};
		await User.updateOne(filter, update);
		return res.status(200).send({response: 'User logged out'})
	};

	editProfile = async (req: any, res: any, next?: any) => {
		try {
			let user;
			// If password is not to be updated
			if (!req.value.body.password) {
				user = await User.findOneAndUpdate(
					{_id: req.user._id},
					{...req.value.body},
					updateOptions
				);
				return res.status(200).send({msg: "User's profile successfully edited", profile: getProfile(user!)});
			}
			// If password needs to be updated - different flow due to pre save middleware (password hashing)
			user = await User.findOne({_id: req.user._id});
			if (user) {
				user = {...user, ...req.user};
				// save updated password
				await user!.save();
				return res.status(200).send({msg: "User's profile successfully edited", profile: getProfile(user!)});
			}
			return res.status(400).send({error: "No user found for editing"});

		} catch (e) {
			logger.error("UserService.editProfile ", e);
			return res.status(400).send({error: "editProfile error"});
		}
	};

	sendResetLink = async (req: any, res: any, next?: any) => {
		try {
			if (req.value.body.email) {

				const user: IUser | null = await User.findOne({email: req.value.body.email});
				// We need to lie about not finding email for security reasons
				if (!user)
					return res.status(200).send({msg: "Reset email sent to provided email"});

				const resetCode = await getResetPasswordCode(user.email);
				await sendgridAdapter.sendPasswordResetEmail(user.email, `worktimeapp://${resetCode}`);
			}
			return res.status(200).send({msg: "Reset email sent to provided email"});
		} catch (e) {
			logger.error("UserService.sendResetLink ", e);
			return res.status(400).send({error: "sendResetLink error"});
		}
	};

	resetPassword = async (req: any, res: any, next?: any) => {
		try {
			const {resetCode, password} = req.value.body;

			// If both required params provided
			if (resetCode && password) {
				const resetToken = await ResetToken.findOneAndDelete({token: resetCode});
				// And password reset token found

				if (!resetToken || !resetToken.isValid())
					return res.status(404).send({msg: "Resource Not Found"});

				const user = await User.findOne({email: resetToken.email});
				// And user found
				if (!user)
					return res.status(404).send({msg: "Resource Not Found"});

				// Update password
				user.password = password;
				// Save updated password (Pre save hook will hash it before saving in DB - see model pre save hook)
				await user!.save();
				return res.status(200).send({msg: "Password reset successfully"});
			}
			return res.status(400).send("Invalid data provided");
		} catch (e) {
			logger.error("UserService.resetPassword ", e);
			return res.status(400).send({error: "resetPassword error"});
		}
	};

	getAllUsers = async (req: any, res: any, next?: any) => {
		try {
			const {role, textSearch} = req.query;
			let filter = {};

			if(role)
				filter = {role};
			if(textSearch)
				filter = {...filter, $text: {$search: textSearch}};


			const users = await User.find(filter);
			const profiles = [];
			// eslint-disable-next-line no-restricted-syntax
			for(const user of users) {
				profiles.push(getProfile(user))
			}

			return res.status(200).send(profiles);
		} catch (e) {
			logger.error("UserService.getAllUsers ", e);
			return res.status(400).send({error: "getAllUsers error"});
		}
	}

};



export default new UserService();
