import User from '../models/User';
import logger from '../config/logger';
import Project from "../models/Project";
import UserRole from "../enums/UserRole";
import {getResetPasswordCode} from "./UserService";
import sendgridAdapter from "../adapters/SendgridAdapter";

export class AdminService {

	banUser = async (req: any, res: any, next?: any) => {
		try {
			const userId = req.value.body.userId;

			if(+req.user.id === userId)
				return res.status(400).send({msg: "Cannot ban yourself!"});

			await User.updateOne({_id: userId}, {role: UserRole.BANNED});
		} catch (e) {
			logger.error("AdminService.ban ", e);
			return res.status(400).json({response: "Banning user error"});
		}

		return res.status(200).json({response: "User has been banned"})
	};

	resetPasswordOfUser = async (req: any, res: any, next?: any) => {
		try {
			const userId = req.value.body.userId;

			await User.updateOne({_id: userId}, {password: "randomHash"});
			const user = await User.findOne({_id: userId});

			if(!user)
				return res.status(400).send({msg: "No user found"});

			const resetCode = await getResetPasswordCode(user.email);
			await sendgridAdapter.sendPasswordResetEmail(user.email, resetCode);

		} catch (e) {
			logger.error("AdminService.resetPasswordOfUser ", e);
			return res.status(400).json({response: "Changing user's password"});
		}

		return res.status(200).json({response: "User's password has been reset"})
	};

	changeRole = async (req: any, res: any, next?: any) => {
		try {
			const {userId, newRole} = req.value.body;
			if(+req.user.id === userId)
				return res.status(400).send({msg: "Cannot edit own role!"});

			await User.updateOne({_id: userId}, {role: newRole.toString().toUpperCase()});

		} catch (e) {
			logger.error("AdminService.changeRole ", e);
			return res.status(400).json({response: "Error changing user's role"});
		}

		return res.status(200).json({response: "User's role has been changed"})
	};

	assignUserToProject = async (req: any, res: any, next?: any) => {
		try {
			const {userId, projectId} = req.value.body;
			const project = await Project.findOne({_id: projectId});
			const user = await User.findOne({_id: userId});

			if (!project || !user)
				return res.status(401).json({response: "user or project is missing"});

			const {id, firstName, lastName} = user;
			const basicUser = {id, firstName, lastName};

			switch (user.role) {
				case UserRole.ADMIN:
				case UserRole.MANAGER:
					project.manager = basicUser;
					await project.save();
					break;
				case UserRole.USER:
					if (!project.developers.find(user => user.id === +id)) {
						project.developers.push(basicUser);
						await project.save();
					} else
						return res.status(200).json({response: "user already in the project"});
					break;
				case
				UserRole.BANNED:
				default:
					return res.status(404).json({response: "user's role is incorrect or user is banned"});
			}

		} catch
			(e) {
			logger.error("AdminService.assignUserToProject ", e);
			return res.status(400).json({response: "Assigning user to project error"});
		}
		return res.status(201).json({response: "User assigned"})

	};

}

export default new AdminService();