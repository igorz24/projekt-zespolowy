import Task from '../models/Task';
import Commit from '../models/Commit';
import logger from '../config/logger';
import ITask from "../types/ITask";
import UserRole from "../enums/UserRole";
import Project from "../models/Project";

export class TasksService {
	createTask = async (req: any, res: any, next?: any) => {
		try {
			const task = new Task({...req.value.body, managerId: req.user.id});
			await task.save()
		} catch (e) {
			logger.error("TasksService.createTask ", e);
			return res.status(400).json({response: "Creating task error"});
		}
		return res.status(200).json({Tresponse: "Task created"})
	};

	deleteTask = async (req: any, res: any, next?: any) => {
		try {
			const {taskId} = req.value.body;
			const managerId = req.user.id;

			const task = await Task.findOneAndDelete({
				_id: taskId, managerId
			});

			if (!task)
				return res.status(400).json({response: "No task found"});

			await Commit.deleteMany({taskId});

		} catch (e) {
			logger.error("TasksService.deleteTask ", e);
			return res.status(400).json({response: "Deleting task error"});
		}
		return res.status(204).json({response: "Task deleted"})
	};

	getAllTasksOfManager = async (req: any, res: any, next?: any) => {
		try {
			const userId = req.user.id;
			const role = req.user.role;

			const {textSearch} = req.query;

			switch (role) {
				case UserRole.ADMIN:
				case UserRole.MANAGER:
					// eslint-disable-next-line no-case-declarations
					let filter: any = {managerId: userId};
					if (textSearch)
						filter = {...filter, $text: {$search: textSearch}};
					// eslint-disable-next-line no-case-declarations
					const tasks: ITask[] = await Task.find(filter);

					if (tasks)
						return res.status(200).json(tasks);
					return res.status(404).json("Not Found");

				case UserRole.USER:
					// eslint-disable-next-line no-case-declarations
					const projects = await Project.find({'developers.id': userId});
					// eslint-disable-next-line no-case-declarations
					const ids: number[] = [];
					projects.forEach(prj => ids.push(prj._id));
					// eslint-disable-next-line no-case-declarations
					const tasks2 = await Task.find({projectId: {$in: ids}});
					return res.status(200).json(tasks2);

				default:
					return res.status(404).json("Not Found");
			}

		} catch (e) {
			logger.error("TasksService.getAllTasksOfManager ", e);
			return res.status(400).json({response: "Getting all tasks error"});
		}
	};

	getAllTasks = async (req: any, res: any, next?: any) => {
		try {

			const {textSearch} = req.query;
			let filter: any = {};

			if (textSearch)
				filter = {...filter, $text: {$search: textSearch}};

			const tasks: ITask[] = await Task.find(filter);
			if (tasks)
				return res.status(200).json(tasks);

		} catch (e) {
			logger.error("TasksService.getAllTasks ", e);
			return res.status(400).json({response: "Getting all tasks error"});
		}
		return res.status(400).json({response: "No task found"});
	};

	updateTask = async (req: any, res: any, next?: any) => {
		try {
			const {taskId, estimationTime, description} = req.value.body;
			const managerId = req.user.id;

			let update = {};
			if (estimationTime)
				update = {estimationTime};
			if (description)
				update = {...update, description};

			const task = await Task.findOneAndUpdate(
				{_id: taskId, managerId},
				update
			);

			if (!task)
				return res.status(400).json({response: "No task found"});

		} catch (e) {
			logger.error("TasksService.updateTask ", e);
			return res.status(400).json({response: "Updating task error"});
		}
		return res.status(200).json({response: "Task updated"})
	};

}

export default new TasksService();