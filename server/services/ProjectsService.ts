import Project from '../models/Project';
import Commit from '../models/Commit';
import logger from '../config/logger';
import Task from "../models/Task";
import UserRole from "../enums/UserRole";
import CommitStatus from "../enums/CommitStatus";

interface Report {
	numberOfDevelopers: number,
	numberOfTasks: number,
	numberOfCommits: number,
	totalEstimatedTime: number, // minutes
	totalTimeSpent: number // minutes
	commitsWaitingForApproval: number
}

export class ProjectsService {

    createProject = async (req: any, res: any, next?: any) => {

        try {
            const project = new Project({...req.value.body});
            await project.save()
        } catch (e) {
            logger.error("ProjectsService.createProject ", e);
            return res.status(400).json({response: "Creating project error"});
        }
        return res.status(200).json({Response: "Project created"})
    };

    deleteProject = async (req: any, res: any, next?: any) => {
        try {
            const {projectId} = req.value.body;

            const project = await Project.findOneAndDelete({_id: projectId});

            if (!project)
                return res.status(400).json({response: "No project found"});

            // delete all related tasks and commits
            await Task.deleteMany({projectId});
            await Commit.deleteMany({projectId});

        } catch (e) {
            logger.error("ProjectsService.deleteProject ", e);
            return res.status(400).json({response: "Deleting project error"});
        }
        return res.status(204).json({response: "Project deleted"})
    };

    updateProject = async (req: any, res: any, next?: any) => {
        try {
            const {description, name, projectId} = req.value.body;

            let update = {};
            if (name)
                update = {name};
            if (description)
                update = {...update, description};

            const project = await Project.findOneAndUpdate(
                {_id: projectId},
                update
            );

            if (!project)
                return res.status(400).json({response: "No project found"});

        } catch (e) {
            logger.error("ProjectsService.updateProject ", e);
            return res.status(400).json({response: "Updating project error"});
        }
        return res.status(200).json({response: "Project updated"})
    };

	getMyProjects = async (req: any, res: any, next?: any) => {
		try {
			const {textSearch} = req.query;
			let filter: any = {'manager.id': req.user.id};

			if(textSearch)
				filter = {...filter, $text: {$search: textSearch}};

			const result = await Project.find(filter);
			return res.status(200).json(result)
		} catch (e) {
			logger.error("ProjectsService.createProject ", e);
			return res.status(400).json({response: "Getting manager's projects error"});
		}
	};

	getAllProjects = async (req: any, res: any, next?: any) => {
		try {

			const {textSearch} = req.query;
			let filter: any = {};

			if(textSearch)
				filter = {...filter, $text: {$search: textSearch}};

			const result = await Project.find(filter);
			return res.status(200).json(result)
		} catch (e) {
			logger.error("ProjectsService.getAllProjects ", e);
			return res.status(400).json({response: "Getting manager's projects error"});
		}
	};

	getProjectDevelopers = async (req: any, res: any, next?: any) => {
		try {
			const result = await Project.findOne({_id: +req.query.projectId,'manager.id': req.user.id});
			if(!result)
				return res.status(400).json({response: "No project matching criteria"});

			return res.status(200).json({Response: result.developers})
		} catch (e) {
			logger.error("ProjectsService.getProjectDevelopers ", e);
			return res.status(400).json({response: "Getting all developers from requested project error"});
		}
	};

	getReport = async (req: any, res: any, next?: any) => {
		try {
			const {projectId} = req.query;
			const managerId = req.user.id;
			const userRole = req.user.role;

			const project = await Project.findOne({_id: projectId});
			if(!project || (project.manager.id !== +managerId && userRole !== UserRole.ADMIN))
				return res.status(400).json({response: "No project or user has no access"});

			const tasks = await Task.find({projectId});
			const commits = await Commit.find({projectId});

			let totalEstimatedTime = 0;
			let totalTimeSpent = 0;
			let commitsWaitingForApproval = 0;

			// eslint-disable-next-line no-restricted-syntax
			for(const task of tasks) {
				totalEstimatedTime += task.estimationTime;
			}

			// eslint-disable-next-line no-restricted-syntax
			for(const commit of commits) {
				totalTimeSpent += commit.workTime;
				if(commit.status === CommitStatus.WAITING_FOR_APPROVAL)
					commitsWaitingForApproval += 1;
			}


			const report: Report = {
				numberOfDevelopers: project.developers.length,
				numberOfCommits: commits.length,
				numberOfTasks: tasks.length,
				totalEstimatedTime,
				totalTimeSpent,
				commitsWaitingForApproval
			};

			return res.status(200).json(report)
		} catch (e) {
			logger.error("ProjectsService.getReport ", e);
			return res.status(400).json({response: "Generating report error"});
		}
	};

}

export default new ProjectsService();