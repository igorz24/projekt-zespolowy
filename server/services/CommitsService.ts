import Commit from '../models/Commit';
import Task from '../models/Task';
import logger from '../config/logger';
import ICommit from "../types/ICommit";
import CommitStatus from "../enums/CommitStatus";
import Project from "../models/Project";
import UserRole from "../enums/UserRole";
import updateOptions from "../models/options/updateOptions";

// TODO check if user is assigned to project, before he does something with the commit

export class CommitService {
	createCommit = async (req: any, res: any, next?: any) => {
		try {
			const taskId = req.value.body.taskId;
			const task = await Task.findOne({_id: taskId});
			if(!task)
				return res.status(400).json({response: "Cannot create commit for non-existing task"});


			const commit = new Commit({...req.value.body,projectId: task.projectId,  authorId: req.user.id});
			await commit.save()
		} catch (e) {
			logger.error("CommitService.createCommit ", e);
			return res.status(400).json({response: "Creating commit error"});
		}
		return res.status(200).json({response: "Commit created"})
	};

	deleteCommit = async (req: any, res: any, next?: any) => {
		try {
			const {commitId} = req.value.body;
			const authorId = req.user.id;

			const commit = await Commit.findOneAndDelete({
				_id: commitId, authorId
			});

			if(!commit)
				return res.status(400).json({response: "No commit found"});

		} catch (e) {
			logger.error("CommitService.deleteCommit ", e);
			return res.status(400).json({response: "Deleting commit error"});
		}
		return res.status(204).json({response: "Commit deleted"})
	};

	getMyAllCommits = async (req: any, res: any, next?: any) => {
		try {
			const authorId = req.user.id;

			const {status, textSearch} = req.query;
			let filter: any = {authorId};

			if(status)
				filter = {...filter, status};
			if(textSearch)
				filter = {...filter, $text: {$search: textSearch}};

			const commits: ICommit[] = await Commit.find(filter);

			if(commits)
				return res.status(200).json(commits);

		} catch (e) {
			logger.error("CommitService.getAllCommits ", e);
			return res.status(400).json({response: "Getting all commits error"});
		}
		return res.status(400).json({response: "No commit found"});
	};

	getAllCommits = async (req: any, res: any, next?: any) => {
		try {
			const {status, textSearch} = req.query;
			let filter = {};

			if(status)
				filter = {status};
			if(textSearch)
				filter = {...filter, $text: {$search: textSearch}};

			const commits: ICommit[] = await Commit.find(filter);

			if(commits)
				return res.status(200).json(commits);

		} catch (e) {
			logger.error("CommitService.getAllCommits ", e);
			return res.status(400).json({response: "Getting all commits error"});
		}
		return res.status(400).json({response: "No commit found"});
	};

	approveCommit = async (req: any, res: any, next?: any) => {
		try {
			const {id, role} = req.user;
			const commitId = req.value.body.commitId;
			let commit;

			if(role === UserRole.ADMIN) {
				commit = await Commit.findOneAndUpdate(
					{_id: commitId},
					{status: CommitStatus.APPROVED},
					updateOptions
				);
			}
			else {
				commit = await Commit.findOne({_id: commitId});

				if(!commit)
					return res.status(400).json({response: "Approving commit error"});
				const project = await Project.findOne({_id: commit.projectId});

				if(!project || !project.manager || project.manager.id !== +id)
					return res.status(400).json({response: "Approving commit error"});

				commit = await Commit.findOneAndUpdate(
					{_id: commitId},
					{status: CommitStatus.APPROVED},
					updateOptions
				);
			}
			return res.status(200).json(commit);
		} catch (e) {
			logger.error("CommitService.approveCommit ", e);
			return res.status(400).json({response: "Approving commit error"});
		}
	};

	getAllCommitsWaitingForApproval = async (req: any, res: any, next?: any) => {
		try {
			const myProjects = await Project.find({'manager.id': req.user.id});
			const projectIds = [];

			// eslint-disable-next-line no-restricted-syntax
			for(const project of myProjects) {
				projectIds.push(project.id)
			}

			const commits: ICommit[] = await Commit.find({
				status: CommitStatus.WAITING_FOR_APPROVAL,
				projectId: {$in: projectIds}
			});

			if(commits)
				return res.status(200).json(commits);

		} catch (e) {
			logger.error("CommitService.getAllCommitsWaitingForApproval ", e);
			return res.status(400).json({response: "Getting all commits error"});
		}
		return res.status(400).json({response: "No commit found"});
	};

	updateCommit = async (req: any, res: any, next?: any) => {
		try {
			const {commitId, workTime, description} = req.value.body;
			const authorId = req.user.id;

			let update = {};
			if(workTime)
				update = {workTime};
			if(description)
				update = {...update, description};

			const commit = await Commit.findOneAndUpdate(
				{_id: commitId, authorId},
				update
				);

			if(!commit)
				return res.status(400).json({response: "No commit found"});

		} catch (e) {
			logger.error("CommitService.updateCommit ", e);
			return res.status(400).json({response: "Updating commit error"});
		}
		return res.status(200).json({response: "Commit updated"})
	};


}

export default new CommitService();