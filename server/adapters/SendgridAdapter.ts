import sgMail from '@sendgrid/mail';
import {MailData} from '@sendgrid/helpers/classes/mail';
import request from 'request-promise';
import logger from '../config/logger';

sgMail.setApiKey(process.env.SENDGRID_API_KEY!);

export class SendgridAdapter {
  sendWelcomeEmail = async (to: string) => {
    if(!to) return;

    try {
      const msg = {
        to,
        from: 'super@projekt.com',
        subject: 'Welcome to Projek Zespołowy by Miketa, Ostrowski & Zuber!',
        text: 'Bang!',
        html: '<strong>Bang!</strong>',
      };

      await sgMail.send(msg);
    } catch(e) {
      logger.error(`SendgridAdapter.sendWelcomeEmail `, e);
    }
  };

  sendPasswordResetEmail = async (to: string, resetCode: string) => {
    if(!to) return;

    try {
      const msg: MailData = {
        to,
        from: 'super@projekt.com',
        subject: 'ResetLink!',
        text: `Skopiuj ten kod do aplikacji, żeby zresetować hasło: ${resetCode}`,
        html: `<strong>Skopiuj ten kod do aplikacji, żeby zresetować hasło: ${resetCode}</strong>`,
      };
      await sgMail.send(msg);
    } catch(e) {
      logger.error(`SendgridAdapter.sendPasswordResetEmail `, e);
    }
  };

  testConnection = async (): Promise<boolean> => {
    try {
      let connectionEstablished: boolean = false;
      await request.get({
        url: `${process.env.SENDGRID_ENDPOINT_URL}/v3/categories`,
        headers: {'Authorization': `Bearer ${process.env.SENDGRID_API_KEY}`}
      }, (err: any, httpResponse: any, respBody: any) => {
        connectionEstablished = httpResponse && httpResponse.statusCode && httpResponse.statusCode === 200;
      });
      return connectionEstablished
    } catch(e) {
      logger.error(`SendgridAdapter.testConnection `, e);
      return false;
    }
  }
}

export default new SendgridAdapter();