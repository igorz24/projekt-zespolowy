/** Declaration file generated by dts-gen */

declare module "rand-token" {

  export function generate(size: any, chars?: string): any;

  export function generator(options: any): any;

  export function suid(length: any, epoch: any, prefixLength: any): any;

  export function uid(size: any, chars?: string): any;
}

