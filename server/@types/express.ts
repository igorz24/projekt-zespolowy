import express from 'express'
import IUser from "../types/IUser";

// \/\/ Adds correctly new fields but fails to override existing ones \/\/
// declare namespace Express {
//   export interface Request {
//     value?: {
//       body?: any
//     },
//   }
// }
// /\/\---------------------------------------------------------------/\/\

export interface CustomRequest extends express.Request {
  value: {
    body?: any
  },
  user: IUser
}