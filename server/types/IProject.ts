import {Document} from 'mongoose';
import {IBasicUser} from "./IUser";

export default interface IProject extends Document {
	name: string,
	manager: IBasicUser,
	developers: IBasicUser[],
	description: string
}