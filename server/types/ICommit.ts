import {Document} from "mongoose";
import CommitStatus from "../enums/CommitStatus";

export default interface ICommit extends Document {
	date: Date;
	workTime: number // minutes
	projectId: number;
	taskId: number;
	authorId: number;
	description: string;
	status: CommitStatus;
}
