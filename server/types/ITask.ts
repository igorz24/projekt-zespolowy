import {Document} from "mongoose";

export default interface ICommit extends Document {
	projectId: number;
	estimationTime: number; // minutes
	description: string;
	managerId: number;
}
