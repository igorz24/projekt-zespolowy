import UserRole from '../enums/UserRole';

export default interface IJwtToken {
    // Issuer - defined in env file
    iss: string,
    // Subject - user id
    sub: string,
    // Expiration date in seconds
    exp: number,
    // Issued at - issue time
    iat: number,
    // JWT ID - unique jwt token id
    jti: string,
    // User role - describes user's permissions
    role: UserRole
}