import { Document } from 'mongoose';
import UserRole from '../enums/UserRole';

export interface IBasicUser {
  id: Number,
  firstName: String,
  lastName: String
}

export default interface IUser extends Document {
  _id: string,
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  lastJwtId: string | undefined;
  role: UserRole;

  isValidPassword(plainPassword: String): Promise<boolean>,

  isValidToken(jwtId: String): boolean,

  isSubscriptionActive(): boolean,

  hasAccess(roleToCheckAgainst: UserRole): boolean,
}