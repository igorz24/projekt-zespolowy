import {Document} from "mongoose";

export default interface IResetToken extends Document {
    _id: string,
    token: string,
    email: string,
    updatedAt: Date,
    createdAt: Date,
    isValid(): boolean
}
