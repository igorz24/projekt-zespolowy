export default interface IPeriod {
  gteDate: number,
  lteDate: number
}