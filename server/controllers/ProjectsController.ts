import passport from 'passport';
import PromiseRouter from 'express-promise-router';
import projectsService from '../services/ProjectsService';
import projectSchemas from '../validators/ProjectSchemas'
import Validator from "../validators/Validator";

// JWT token Strategies
const passportJwtAdmin = passport.authenticate('token-admin', {session: false});
const passportJwtManager = passport.authenticate('token-manager', {session: false});
const router = PromiseRouter();

router.route('/mine').get(
	// Authorize user
	passportJwtManager,
	// Get all my projects
	projectsService.getMyProjects
);

router.route('/report').get(
	// Authorize user
	passportJwtManager,
	// Get all my projects
	projectsService.getReport
);

router.route('/all').get(
	// Authorize user
	passportJwtAdmin,
	// Get all my projects
	projectsService.getAllProjects
);

router.route('/developers').get(
	// Authorize user
	passportJwtManager,
	// Get all my projects
	projectsService.getProjectDevelopers
);

router.route('').post(
	// Validate if commit data is provided
	Validator.validateBody(projectSchemas.create),
	// Authorize user
	passportJwtAdmin,
	// Create project
	projectsService.createProject
);

router.route('').delete(
	// Validate if commitId is provided
	Validator.validateBody(projectSchemas.projectId),
	// Authorize user
	passportJwtAdmin,
	// Delete project
	projectsService.deleteProject
);

router.route('').patch(
	// Validate if exactly one parameter is provided {email, tokenID}
	Validator.validateBody(projectSchemas.update),
	// Authorize user
	passportJwtAdmin,
	// Update project
	projectsService.updateProject
);

router.route('').get(
	// Authorize user
	passportJwtAdmin,
	// Get all projects
	projectsService.getAllProjects
);

export default router;