import passport from 'passport';
import PromiseRouter from 'express-promise-router';
import userService from '../services/UserService';
import userSchemas from '../validators/UserSchemas'
import Validator from "../validators/Validator";

// Login and Password Strategy
const passportLocal = passport.authenticate('local', {session: false});
// JWT token Strategies
const passportJwtUser = passport.authenticate('token', {session: false});
const passportJwtManager = passport.authenticate('token-manager', {session: false});
const passportJwtAdmin = passport.authenticate('token-admin', {session: false});

const router = PromiseRouter();

router.route('/').get(
	// Only admin can do that
	passportJwtAdmin,
	// Create user and return token
	userService.getAllUsers
);

router.route('/signup').post(
	// Validate request body
	Validator.validateBody(userSchemas.signUp),
	// Create user and return token
	userService.signUp
);

router.route('/signin').post(
	// Validate request body
	Validator.validateBody(userSchemas.signIn),
	// Validate password
	passportLocal,
	// Generate token
	userService.signIn
);

router.route('/secret').get(
	// Find user specified in token
	passportJwtUser,
	// Return secured resource
	userService.secret
);

router.route('/secret/manager').get(
	// Find user specified in token
	passportJwtManager,
	// Return secured resource
	userService.secret
);

router.route('/secret/admin').get(
	// Find user specified in token
	passportJwtAdmin,
	// Return secured resource
	userService.secret
);

router.route('/logout').get(
	// Find user specified in token
	passportJwtUser,
	// Logout
	userService.logout
);

router.route('/edit').post(
	// Validate request body
	Validator.validateBody(userSchemas.editProfile),
	// Authorization
	passportJwtUser,
	// Edit user profile
	userService.editProfile
);

router.route('/reset/code').post(
	// Validate request
	Validator.validateBody(userSchemas.sendResetLink),
	// Send password reset link
	userService.sendResetLink
);

router.route('/reset/password').post(
	// Validate request
	Validator.validateBody(userSchemas.resetPassword),
	// Reset password
	userService.resetPassword
);


export default router;