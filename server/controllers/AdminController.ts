import passport from 'passport';
import PromiseRouter from 'express-promise-router';
import adminService from '../services/AdminService';
import adminSchemas from '../validators/AdminSchemas'
import Validator from "../validators/Validator";

// JWT token Strategies
const passportJwtAdmin = passport.authenticate('token-admin', {session: false});
// Router config
const router = PromiseRouter();

router.route('/ban').post(
  // Validate if exactly one parameter is provided {email, tokenID}
  Validator.validateBody(adminSchemas.ban),
  // Authorize admin
  passportJwtAdmin,
  // Invalidate user's tokens
  adminService.banUser
);

router.route('/password/reset').post(
	// Validate if exactly one parameter is provided {email, tokenID}
	Validator.validateBody(adminSchemas.resetUsersPassword),
	// Authorize admin
	passportJwtAdmin,
	// Invalidate user's tokens
	adminService.resetPasswordOfUser
);

router.route('/assign').post(
	// Validate if exactly one parameter is provided {email, tokenID}
	Validator.validateBody(adminSchemas.assignToProject),
	// Authorize admin
	passportJwtAdmin,
	// Invalidate user's tokens
	adminService.assignUserToProject
);

router.route('/role').patch(
	// Validate if exactly one parameter is provided {email, tokenID}
	Validator.validateBody(adminSchemas.changeRole),
	// Authorize admin
	passportJwtAdmin,
	// Invalidate user's tokens
	adminService.changeRole
);

export default router;