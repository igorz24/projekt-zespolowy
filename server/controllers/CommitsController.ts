import passport from 'passport';
import PromiseRouter from 'express-promise-router';
import commitService from '../services/CommitsService';
import commitSchemas from '../validators/CommitSchemas'
import Validator from "../validators/Validator";

// JWT token Strategies
const passportJwtUser = passport.authenticate('token', {session: false});
const passportJwtManager = passport.authenticate('token-manager', {session: false});
const passportJwtAdmin = passport.authenticate('token-admin', {session: false});
const router = PromiseRouter();

router.route('').post(
	// Validate if commit data is provided
	Validator.validateBody(commitSchemas.create),
	// Authorize user
	passportJwtUser,
	// Create commit
	commitService.createCommit
);

router.route('').delete(
	// Validate if commitId is provided
	Validator.validateBody(commitSchemas.commitId),
	// Authorize user
	passportJwtUser,
	// Delete commit
	commitService.deleteCommit
);

router.route('').get(
	// Authorize user
	passportJwtUser,
	// Get all commits of authorized user
	commitService.getMyAllCommits
);

router.route('/all').get(
	// Authorize user
	passportJwtAdmin,
	// Get all commits of authorized user
	commitService.getAllCommits
);

router.route('/waiting').get(
	// Authorize user
	passportJwtManager,
	// Get all commits of authorized user
	commitService.getAllCommitsWaitingForApproval
);

router.route('/approve').post(
	// Validate if commitId is provided
	Validator.validateBody(commitSchemas.commitId),
	// Authorize user
	passportJwtManager,
	// Get all commits of authorized user
	commitService.approveCommit
);

router.route('').patch(
	// Validate if exactly one parameter is provided {email, tokenID}
	Validator.validateBody(commitSchemas.update),
	// Authorize user
	passportJwtUser,
	// Update commit
	commitService.updateCommit
);

export default router;