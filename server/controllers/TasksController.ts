import passport from 'passport';
import PromiseRouter from 'express-promise-router';
import tasksService from '../services/TasksService';
import taskSchemas from '../validators/TaskSchemas'
import Validator from "../validators/Validator";

// JWT token Strategies
const passportJWTUser = passport.authenticate('token', {session: false});
const passportJwtManager = passport.authenticate('token-manager', {session: false});
const passportJwtAdmin = passport.authenticate('token-admin', {session: false});
const router = PromiseRouter();

router.route('').post(
	// Validate if commit data is provided
	Validator.validateBody(taskSchemas.create),
	// Authorize user
	passportJwtManager,
	// Create task
	tasksService.createTask
);

router.route('').delete(
	// Validate if commitId is provided
	Validator.validateBody(taskSchemas.taskId),
	// Authorize user
	passportJwtManager,
	// Delete task
	tasksService.deleteTask
);

router.route('').get(
	// Authorize user
	passportJWTUser,
	// Get all tasks of authorized manager
	tasksService.getAllTasksOfManager
);

router.route('/all').get(
	// Authorize user
	passportJwtAdmin,
	// Get all tasks of authorized manager
	tasksService.getAllTasks
);

router.route('').patch(
	// Validate if exactly one parameter is provided {email, tokenID}
	Validator.validateBody(taskSchemas.update),
	// Authorize user
	passportJwtManager,
	// Update task
	tasksService.updateTask
);

export default router;