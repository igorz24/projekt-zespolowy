enum UserRole {
  BANNED = "BANNED",
  USER = "USER",
  MANAGER = "MANAGER",
  ADMIN = "ADMIN"
}

export const roleValues = {
  BANNED: -1,
  USER: 0,
  MANAGER: 1,
  ADMIN: 2
};

export const UserRoles: Array<string> = [
  UserRole.BANNED,
  UserRole.USER,
  UserRole.MANAGER,
  UserRole.ADMIN
];

export default UserRole;