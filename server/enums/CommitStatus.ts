enum CommitStatus {
	WAITING_FOR_APPROVAL = "waiting",
	APPROVED = "approved"
}

export default CommitStatus;