import {PassportStatic} from "passport";
import passportJWT from 'passport-jwt';
import passportLocal from 'passport-local';
import IJwtToken from "../types/IJwtToken";
import UserRole from "../enums/UserRole";
import User from '../models/User';

const JwtStrategy = passportJWT.Strategy;
const {ExtractJwt} = passportJWT;
const LocalStrategy = passportLocal.Strategy;

// JSON WEB TOKENS STRATEGY
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.SECRET
};

const getJwtStrategyWithAccessLevel = (accessLevel: UserRole) => {
  return new JwtStrategy(
    jwtOptions, async (payload: IJwtToken, done: Function) => {
      try {
        // Find user specified in token (Header + Payload + Signature)
        // sub == Subject
        const user = await User.findById(payload.sub);
        // If user doesn't exist or
        // If token is different than last active, handle it
        if(!user || !user.hasAccess(accessLevel) || !user.isValidToken(payload.jti))
          return done(null, false);

        // Otherwise return the user
        return done(null, user);

      } catch(error) {
        return done(error, false)
      }
    })
};


const initializePassport = (passport : PassportStatic) => {

  passport.use('token',
    getJwtStrategyWithAccessLevel(UserRole.USER)
  );

  passport.use('token-manager',
    getJwtStrategyWithAccessLevel(UserRole.MANAGER)
  );

  passport.use('token-admin',
    getJwtStrategyWithAccessLevel(UserRole.ADMIN)
  );

  // LOCAL STRATEGY
  const localOptions = {
    usernameField: 'email'
  };

  passport.use('local',
    new LocalStrategy(
      localOptions, async (email: String, password: String, done: Function) => {
        try {

          // Find the user by email
          const user = await User.findOne({email});
          // If no user found, or
          // Check if the password is correct
          // If not, handle it
          if(!user || !await user.isValidPassword(password) || !user.hasAccess(UserRole.USER))
            return done(null, false);

          // Otherwise, return the user
          return done(null, user);

        } catch(error) {
          return done(error, false)
        }
      })
  );

};

export default initializePassport;


