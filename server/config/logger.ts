import winston, {format} from "winston";
import dotenv from 'dotenv';
import * as morgan from "morgan";

const {combine} = format;

dotenv.config({path: `env.${process.env.NODE_ENV}`});

const logger = winston.createLogger({
  silent: process.env.ENV === "test",
  transports: [
    new winston.transports.Console({
      format: combine(
        format.colorize(),
        format.simple(),
        format.timestamp()
      ),
      level: 'debug',
      handleExceptions: true,
    })
    // new winston.transports.File({
    //   format: format.combine(timestamp()),
    //   level: 'info',
    //   filename: './logs/combined.log',
    //   handleExceptions: true,
    //   maxsize: 5242880, // 5MB
    //   maxFiles: 5,
    // }),

  ],
  exitOnError: false
});

export default logger;
export const stream: morgan.StreamOptions  = {
  write(message: any){
    logger.info(message);
  }
};