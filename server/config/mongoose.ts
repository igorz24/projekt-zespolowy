import mongoose from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
import logger from './logger';

autoIncrement.initialize(mongoose.connection);

const options = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  bufferMaxEntries: 0,
  poolSize: 5,
  useUnifiedTopology: true,
  autoReconnect: false,
  connectTimeoutMS: 5000, // Give up initial connection after X seconds
  socketTimeoutMS: 10000, // Close sockets after X seconds of inactivity (inactive or taking too long)
  keepAlive: false
};

const db = mongoose.connection;

db.on('connecting', () => {
  logger.info('connecting to MongoDB...');
});

db.on('connected', () => {
  logger.info('MongoDB connected!');
});

db.on('reconnectedFailed', () => {
  logger.info('reconnectedFailed');
});

db.on('reconnected', () => {
  logger.info('MongoDB reconnected!');
});

db.on('disconnecting', () => {
  logger.info('disconnecting MongoDB...');
});

db.on('disconnected', async () => {
  logger.info('MongoDB disconnected!');
});

db.on('error', async (error: Error) => {
  logger.error(`Error in MongoDb connection: ${error}`);
});

db.on('timeout', () => {
  logger.info('timeout');
});

db.on('open', () => {
  logger.info('MongoDB connection opened!');
});

db.on('close', () => {
  logger.info('MongoDB connection closed!');
});



export default { initialize: async () => mongoose.connect(process.env.MONGODB_URI!, options)}

