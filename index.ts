import dotenv from 'dotenv';
import mongo from './server/config/mongoose';
import logger from './server/config/logger';
import app from './server/app';

// Set up environmental variables
dotenv.config({path: `env.${process.env.NODE_ENV}`});

mongo.initialize().catch((err: Error) => {
  logger.error(`Failed the initial connection to MongoDB: ${err}`);
});

app.listen(process.env.PORT, () => {
  logger.info(`Server listening on port ${process.env.PORT}!`);
});