//
//  AppDelegate.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import UIKit

extension UIApplication {

    static func appDelegate() -> AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    // MARK: - Properties
    
    var rootRouter: RootRouter?
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let rootVC = RootViewController()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootVC
        window?.makeKeyAndVisible()
        
        rootRouter = RootRouter(rootViewController: rootVC, securityRepository: SecurityFactory.repository())
        rootRouter?.start()
        
        return true
    }
    
    func application(_ application: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any] = [:] ) -> Bool {
        
        
        rootRouter?.routeToChangePassword(code: url.host ?? "")
        
        return true
    }
}

