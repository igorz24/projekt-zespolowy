//
//  AppError.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct AppError: Error {
    
    var message: String
    
    init(message: String) {
        self.message = message
    }
    
    init(error: Error) {
        self.message = error.localizedDescription
    }
}
