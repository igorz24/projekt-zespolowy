//
//  EnvConfig.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct EnvConfig {
    
    // MARK: - Init
    
    private init() {}
    
    
    // MARK: - Properties
    
    private static var info: [String: Any] {
        return Bundle.main.infoDictionary ?? [:]
    }
    
    static var appName: String {
        return stringValueFromInfo(by: "AppName")
    }
    
    static var appVersion: String {
        return stringValueFromInfo(by: "AppVersion")
    }
    
    static var appVersionBuild: String {
        return stringValueFromInfo(by: "AppVersionBuild")
    }
        
    static var ApiHost: String {
        return stringValueFromInfo(by: "Host")
    }
    
    static var BaseURL: String {
        let apiProtocol = stringValueFromInfo(by: "Protocol")
        let apiHost = stringValueFromInfo(by: "Host")
        return "\(apiProtocol)://\(apiHost)"
    }
            
    static var appBundleId: String {
        return stringValueFromInfo(by: "CFBundleIdentifier")
    }
    
    static var logLevel: LogLevel {
        guard let logLevel = LogLevel(rawValue: stringValueFromInfo(by: "LogLevel").lowercased()) else {
            return .off
        }
        
        return logLevel
    }
    
    
    // MARK: - Methods
    
    private static func stringValueFromInfo(by key: String) -> String {
        guard let string = info[key] as? String else {
            return ""
        }
        
        return string
    }
}

