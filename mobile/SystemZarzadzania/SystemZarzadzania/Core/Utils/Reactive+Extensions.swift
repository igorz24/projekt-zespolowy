//
//  Reactive+Extensions.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit
import Alamofire

extension Request: ReactiveCompatible {}

extension Reactive where Base: DataRequest {

    func dataResponse() -> Single<Data> {
        return Single.create { observer in
            let request = self.base.responseData { responseData in
                switch responseData.result {
                case .success(let value):
                    observer(.success(value))
                case .failure(let error):
                    observer(.error(ErrorMapping.mapError(responseData: responseData, error: error)))
                }
            }

            return Disposables.create(with: { request.cancel() })
        }
    }
    
//    func headerResponse(headerName: String) -> Single<String> {
//        return Single.create { observer in
//            let request = self.base.responseData { responseData in
//                switch responseData.result {
//                case .success:
//                    guard let headers = responseData.response?.allHeaderFields, let header = headers[headerName] as? String else {
//                        observer(.error(AppError.technical(error: TechnicalError.alamofireNoData)))
//                        return
//                    }
//
//                    observer(.success(header))
//                case .failure(let error):
//                    observer(.error(ErrorMapping.mapError(responseData: responseData, error: error)))
//                }
//            }
//
//            return Disposables.create(with: request.cancel)
//        }
//    }
}
