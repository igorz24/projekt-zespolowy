//
//  Observable+Extensions.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

extension PrimitiveSequenceType where Trait == SingleTrait, Element == Data {
        
    func decodeObject<T: Decodable>(ofType type: T.Type) -> PrimitiveSequence<Trait, T> {
        return self.map { data -> T in
            guard data.count > 0 else {
                throw AppError(message: "Alamofire error: Empty data")
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .custom({ try CoreDateFormatter.dateFromCustomString(decoder: $0) })
                
                let object = try decoder.decode(T.self, from: data)
                
                return object
            } catch {
                Logger.log(error: error)
                throw self.decodeError(error)
            }
        }
    }
    
    func decodeArray<T: Decodable>(ofType type: T.Type) -> PrimitiveSequence<Trait, [T]> {
        return self.map { data -> [T] in
            guard data.count > 0 else {
                throw AppError(message: "Alamofire error: Empty data")
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .custom({ try CoreDateFormatter.dateFromCustomString(decoder: $0) })

                let object = try decoder.decode([T].self, from: data)
                return object
            } catch {
                Logger.log(error: error)
                throw self.decodeError(error)
            }
        }
    }
    
    private func decodeError(_ error: Error) -> Error {
        if let decodingError = error as? DecodingError, case let DecodingError.dataCorrupted(context) = decodingError {
            let codingPathExpanded = context.codingPath.reduce(into: "") { (result, codingKey) in
                result = "\(result) -> \(codingKey.debugDescription)"
            }
            
            return AppError(message: "Cause: \(context.debugDescription) | Details: \(codingPathExpanded)")
        } else {
            return AppError(error: error)
        }
    }
}
