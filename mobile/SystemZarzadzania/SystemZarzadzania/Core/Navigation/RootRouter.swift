//
//  RootRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import UIKit

class RootRouter {

    // MARK: - Properties

    let rootViewController: RootViewController
    let securityRepository: SecurityRepositoryProtocol


    // MARK: - Init

    init(rootViewController: RootViewController, securityRepository: SecurityRepositoryProtocol) {
        self.rootViewController = rootViewController
        self.securityRepository = securityRepository
    }


    // MARK: - Methods

    func start() {
        if securityRepository.isAuthorized() {
            routeToMain()
        } else {
            routeTo(UIStoryboard.viewController(for: .login))
        }
    }
    
    func routeToChangePassword(code: String) {
        guard let destination = UIStoryboard.viewController(for: .login) as? LoginViewController ,let destination2 = UIStoryboard.viewController(for: .resetPassword) as? ResetPasswordViewController, var dataStore = destination2.router?.dataStore else {
            return
        }
        
        passData(code: code, dataStore: &dataStore)

        routeTo([destination, destination2])
    }
    
    func passData(code: String, dataStore: inout ResetPasswordDataStore) {
        dataStore.code = code
    }
    
    func routeToMain() {
        let vc = MainViewController()
        
        routeTo(vc)
    }

    func routeTo(_ vc: UIViewController) {
        routeTo([vc])
    }

    func routeTo(_ vcStack: [UIViewController]) {
        rootViewController.setViewControllers(vcStack, animated: true)
    }
    

    func routeToLogin() {
        guard let destination = UIStoryboard.viewController(for: .login) as? LoginViewController else {
            return
        }
        
        routeTo(destination)
    }
}

