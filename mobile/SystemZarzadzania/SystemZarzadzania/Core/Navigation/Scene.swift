//
//  Scene.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum Scene: String {
    case login = "Login"
    case resetPassword = "ResetPassword"
    case reigster = "Register"
    case commit = "Commit"
    case addCommit = "AddCommit"
    case task = "Task"
    case addTask = "AddTask"
    case project = "Project"
    case addProject = "AddProject"
    case projectDetails = "ProjectDetails"
    case projectReport = "ProjectReport"
    case user = "User"
    case addUserToProject = "AddUserToProject"
    case more = "More"

    var storyboardName: String {
        switch self {
        default:
            return rawValue
        }
    }
}
