//
//  UIStoryboard+Extension.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    
    static func viewController(for scene: Scene) -> UIViewController {
        let storyboard = UIStoryboard(name: scene.storyboardName, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "\(scene.rawValue)ViewController")
        return vc
    }
}
