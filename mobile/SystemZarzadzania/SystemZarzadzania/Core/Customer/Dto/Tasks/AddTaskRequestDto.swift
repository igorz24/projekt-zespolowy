//
//  AddTaskRequestDto.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct AddTaskRequestDto: Encodable {
    let projectId: Int
    let estimationTime: Int
    let description: String
}
