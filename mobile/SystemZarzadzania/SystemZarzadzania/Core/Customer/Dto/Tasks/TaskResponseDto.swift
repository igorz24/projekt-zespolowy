
//
//  CommitResponseDto.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct TaskResponseDto: Decodable {
    let _id: Int
    let description: String
    let projectId: Int
    let managerId: Int
    let estimationTime: Int
}

