//
//  UpdateCommitRequest.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct UpdateCommitRequestDto: Encodable {
    let commitId: Int
    let workTime: Int
    let description: String
}
