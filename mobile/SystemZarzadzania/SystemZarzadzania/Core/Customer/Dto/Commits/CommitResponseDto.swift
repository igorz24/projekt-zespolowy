//
//  CommitResponseDto.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum CommitStatus: String {
    case waiting = "waiting"
    case approved = "approved"
}

struct CommitResponseDto: Decodable {
    let _id: Int
    let status: String
    let workTime: Int
    let taskId: Int
    let description: String
    let projectId: Int
    let authorId: Int
    let date: Date
}
