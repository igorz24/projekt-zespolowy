//
//  ApproveCommitRequestDto.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct ApproveCommitRequestDto: Encodable {
    let commitId: Int
}
