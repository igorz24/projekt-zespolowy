
//
//  CommitResponseDto.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct ProjectResponseDto: Decodable {
    let _id: Int
    let manager: Manager?
    let name: String
    let description: String
    let developers: [Developers]
}

struct Manager: Decodable {
    let id: Int
    let firstName: String
    let lastName: String
}

struct Developers: Decodable {
    let id: Int
    let _id: String
    let firstName: String
    let lastName: String
}

