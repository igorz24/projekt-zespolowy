//
//  GenerateReportResponseDto.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct GenerateReportResponseDto: Decodable {
    let numberOfDevelopers: Int
    let numberOfCommits: Int
    let numberOfTasks: Int
    let totalEstimatedTime: Int
    let totalTimeSpent: Int
    let commitsWaitingForApproval: Int
}
