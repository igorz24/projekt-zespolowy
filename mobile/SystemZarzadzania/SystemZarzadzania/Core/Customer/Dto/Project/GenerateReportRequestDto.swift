//
//  GenerateReportRequestDto.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct GenerateReportRequestDto: Encodable {
    let projectId: Int
}
