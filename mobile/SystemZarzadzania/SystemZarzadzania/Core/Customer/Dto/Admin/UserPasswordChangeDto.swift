//
//  UserPasswordChangeDto.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct UserPasswordChangeDto: Encodable {
    let userId: Int
}
