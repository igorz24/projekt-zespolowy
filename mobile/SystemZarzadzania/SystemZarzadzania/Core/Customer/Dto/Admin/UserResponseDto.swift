//
//  UserResponseDto.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct UserResponseDto: Decodable {
    let id: Int
    let email: String
    let role: String
    let firstName: String
    let lastName: String
}
