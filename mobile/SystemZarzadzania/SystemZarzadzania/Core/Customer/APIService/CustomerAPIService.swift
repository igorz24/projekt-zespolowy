//
//  CustomerAPIService.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

class CustomerAPIService: CustomerAPIServiceProtocol {
    
    // MARK: - Properties
    
    let restService: RestServiceProtocol
    
    
    // MARK: - Init
    
    init(restService: RestServiceProtocol) {
        self.restService = restService
    }
    
    
    // MARK: - Login
    
    func addCommit(dto: AddCommitRequestDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.addCommit(dto: dto))
    }
    
    func deleteCommit(dto: DeleteCommitRequestDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.deleteCommit(dto: dto))
    }
    
    func updateCommit(dto: UpdateCommitRequestDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.updateCommit(dto: dto))
    }
    
    func getCommitWaiting() -> Single<[CommitResponseDto]> {
        return restService.getForArray(router: CustomerAPIRouter.getCommitWaiting)
    }
    
    func approveCommit(dto: ApproveCommitRequestDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.approveCommit(dto: dto))
    }
    
    func getCommit(dto: CommitRequestDto) -> Single<[CommitResponseDto]> {
        return restService.getForArray(router: CustomerAPIRouter.getCommits(dto: dto))
    }
    
    func getAllCommits(dto: CommitRequestDto) -> Single<[CommitResponseDto]> {
        return restService.getForArray(router: CustomerAPIRouter.getAllCommits(dto: dto))
    }
    
    func addTask(dto: AddTaskRequestDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.addTask(dto: dto))
    }
    
    func deleteTask(dto: DeleteTaskRequestDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.deleteTask(dto: dto))
    }
    
    
    func updateTask(dto: ModifyTaskRequestDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.modifyTask(dto: dto))
    }
    
    func getTask(dto: TaskRequestDto) -> Single<[TaskResponseDto]> {
        return restService.getForArray(router: CustomerAPIRouter.getTask(dto: dto))
    }
    
    func getAllTask(dto: TaskRequestDto) -> Single<[TaskResponseDto]> {
        return restService.getForArray(router: CustomerAPIRouter.getAllTask(dto: dto))
    }
    
    func getProjects(dto: ProjectRequestDto) -> Single<[ProjectResponseDto]> {
        return restService.getForObject(router: CustomerAPIRouter.getProjects(dto: dto))
    }
    
    func modifyProject(dto: ModifyProjectRequestDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.modifyProject(dto: dto))
    }
    
    func addProject(dto: AddProjectRequestDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.addProject(dto: dto))
    }
    
    func deleteProject(dto: DeleteProjectRequestDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.deleteProject(dto: dto))
    }
    
    func getMyProjects(dto: ProjectRequestDto) -> Single<[ProjectResponseDto]> {
        return restService.getForArray(router: CustomerAPIRouter.getMyProjects(dto: dto))
    }
    
    func generateReport(dto: GenerateReportRequestDto) -> Single<GenerateReportResponseDto> {
        return restService.getForObject(router: CustomerAPIRouter.generateReport(dto: dto))
    }
    
    func ban(dto: BanDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.ban(dto: dto))
    }
    
    func changeUserPassword(dto: UserPasswordChangeDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.changeUserPassword(dto: dto))
    }
    
    func getUsers() -> Single<[UserResponseDto]> {
        return restService.getForArray(router: CustomerAPIRouter.getUsers)
    }
    
    func addUserToProject(dto: AddUserToProjectDto) -> Completable {
        return restService.getForEmpty(router: CustomerAPIRouter.addUserToProject(dto: dto))
    }

}
