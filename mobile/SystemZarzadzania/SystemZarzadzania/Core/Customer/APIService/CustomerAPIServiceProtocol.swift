//
//  CustomerAPIServiceProtocol.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol CustomerAPIServiceProtocol {
    func getCommit(dto: CommitRequestDto) -> Single<[CommitResponseDto]>
    func getAllCommits(dto: CommitRequestDto) -> Single<[CommitResponseDto]>
    func getCommitWaiting() -> Single<[CommitResponseDto]>
    func deleteCommit(dto: DeleteCommitRequestDto) -> Completable
    func updateCommit(dto: UpdateCommitRequestDto) -> Completable
    func addCommit(dto: AddCommitRequestDto) -> Completable
    func approveCommit(dto: ApproveCommitRequestDto) -> Completable
    
    func getTask(dto: TaskRequestDto) -> Single<[TaskResponseDto]>
    func getAllTask(dto: TaskRequestDto) -> Single<[TaskResponseDto]>
    func deleteTask(dto: DeleteTaskRequestDto) -> Completable
    func updateTask(dto: ModifyTaskRequestDto) -> Completable
    func addTask(dto: AddTaskRequestDto) -> Completable
    
    func getMyProjects(dto: ProjectRequestDto) -> Single<[ProjectResponseDto]>
    func getProjects(dto: ProjectRequestDto) -> Single<[ProjectResponseDto]>
    func addProject(dto: AddProjectRequestDto) -> Completable
    func modifyProject(dto: ModifyProjectRequestDto) -> Completable
    func deleteProject(dto: DeleteProjectRequestDto) -> Completable
    func generateReport(dto: GenerateReportRequestDto) -> Single<GenerateReportResponseDto>
    
    func ban(dto: BanDto) -> Completable
    func changeUserPassword(dto: UserPasswordChangeDto) -> Completable
    func getUsers() -> Single<[UserResponseDto]>
    func addUserToProject(dto: AddUserToProjectDto) -> Completable
}
