//
//  CustomerFactory.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

final class CustomerFactory {

    // MARK: - Init

    private init() {}


    // MARK: - Methods

    static func apiService() -> CustomerAPIServiceProtocol {
        return CustomerAPIService(restService: RestServiceFactory.restService())
    }

}
