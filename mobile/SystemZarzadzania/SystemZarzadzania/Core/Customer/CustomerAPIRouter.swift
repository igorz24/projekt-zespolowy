//
//  CustomerAPIRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import Alamofire

enum CustomerAPIRouter: BaseAPIRouter {
    case getCommits(dto: CommitRequestDto)
    case getAllCommits(dto: CommitRequestDto)
    case addCommit(dto: AddCommitRequestDto)
    case updateCommit(dto: UpdateCommitRequestDto)
    case deleteCommit(dto: DeleteCommitRequestDto)
    case getCommitWaiting
    case approveCommit(dto: ApproveCommitRequestDto)
    
    case addTask(dto: AddTaskRequestDto)
    case modifyTask(dto: ModifyTaskRequestDto)
    case deleteTask(dto: DeleteTaskRequestDto)
    case getTask(dto: TaskRequestDto)
    case getAllTask(dto: TaskRequestDto)
    
    case getMyProjects(dto: ProjectRequestDto)
    case generateReport(dto: GenerateReportRequestDto)
    case deleteProject(dto: DeleteProjectRequestDto)
    case getProjects(dto: ProjectRequestDto)
    case modifyProject(dto: ModifyProjectRequestDto)
    case addProject(dto: AddProjectRequestDto)
    
    case ban(dto: BanDto)
    case changeUserPassword(dto: UserPasswordChangeDto)
    case getUsers
    case addUserToProject(dto: AddUserToProjectDto)
    
    

    func method() -> HTTPMethod {
        switch self {
        case .getCommits, .getTask, .getMyProjects, .generateReport, .getCommitWaiting, .getProjects, .getAllTask, .getUsers, .getAllCommits:
            return .get
        case .addCommit, .addTask, .approveCommit, .addProject, .ban, .changeUserPassword, .addUserToProject:
            return .post
        case .updateCommit, .modifyTask, .modifyProject:
            return .patch
        case .deleteCommit, .deleteTask, .deleteProject:
            return .delete
        }
    }
        
    func path() -> String {
        switch self {
        case .getCommits, .addCommit, .deleteCommit, .updateCommit:
            return "/commits"
        case .getAllCommits:
            return "/commits/all"
        case .getTask, .deleteTask, .modifyTask, .addTask:
            return "/tasks"
        case .getMyProjects:
            return "/projects/mine"
        case .generateReport:
           return "/projects/report"
        case .getCommitWaiting:
            return "/commits/waiting"
        case .approveCommit:
            return "/commits/approve"
        case .deleteProject, .getProjects, .modifyProject, .addProject:
            return "/projects"
        case .getAllTask:
            return "/tasks/all"
        case .ban:
            return "/admin/ban"
        case .changeUserPassword:
            return "/admin/password/reset"
        case .getUsers:
            return "/users"
        case .addUserToProject:
            return "/admin/assign"
        }
    }
    
    func parameters() -> Parameters? {
        switch self {
        case .getCommits(let dto):
            return prepareParameters(object: dto)
        case .addCommit(let dto):
            return prepareParameters(object: dto)
        case .updateCommit(let dto):
            return prepareParameters(object: dto)
        case .deleteCommit(let dto):
            return prepareParameters(object: dto)
        case .addTask(let dto):
            return prepareParameters(object: dto)
        case .modifyTask(let dto):
            return prepareParameters(object: dto)
        case .getTask(let dto):
            return prepareParameters(object: dto)
        case .deleteTask(let dto):
            return prepareParameters(object: dto)
        case .getMyProjects(let dto):
            return prepareParameters(object: dto)
        case .generateReport(let dto):
            return prepareParameters(object: dto)
        case .approveCommit(let dto):
            return prepareParameters(object: dto)
        case .deleteProject(let dto):
            return prepareParameters(object: dto)
        case .getProjects(let dto):
            return prepareParameters(object: dto)
        case .modifyProject(let dto):
            return prepareParameters(object: dto)
        case .addProject(let dto):
            return prepareParameters(object: dto)
        case .getAllTask(let dto):
            return prepareParameters(object: dto)
        case .ban(let dto):
            return prepareParameters(object: dto)
        case .changeUserPassword(let dto):
            return prepareParameters(object: dto)
        case .addUserToProject(let dto):
            return prepareParameters(object: dto)
        case .getAllCommits(let dto):
            return prepareParameters(object: dto)
        default:
            return nil
        }
    }
    
    func encoding() -> ParameterEncoding {
        switch self {
        case .getCommits, .getTask, .getMyProjects, .generateReport, .getProjects, .getAllTask, .getAllCommits:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    
    func requiresAuthorization() -> Bool {
        return true
    }
}

