//
//  OAuth2Handler.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import Alamofire

class OAuth2Handler: RequestAdapter, RequestRetrier {
    
    // MARK: - Properties
    
    let securityRepository: SecurityRepositoryProtocol
    let sessionManager: Session = RestServiceFactory.session() // OAuth2Handler needs own instance of session manager
    private let lock = NSLock()
    private var isRefreshing = false
    private var requestsToRetry: [(RetryResult) -> Void] = []
    
    
    // MARK: - Init
    
    init(securityRepository: SecurityRepositoryProtocol) {
        self.securityRepository = securityRepository
    }
    
    
    // MARK: - Methods
    
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        var urlRequest = urlRequest
        
        guard let authData = securityRepository.loadAuthData() else {
            completion(.success(urlRequest))
            return
        }
        
        urlRequest.setValue("Bearer \(authData.token)", forHTTPHeaderField: "Authorization")
        completion(.success(urlRequest))
        return
    }
        
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        lock.lock()
        defer {
            lock.unlock()
        }

        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            requestsToRetry.append(completion)

            if !isRefreshing {
                refreshToken { [weak self] (succeeded, token) in
                    guard let strongSelf = self else { return }

                    strongSelf.lock.lock()
                    defer {
                        strongSelf.lock.unlock()
                    }

                    if let token = token {
                        try? strongSelf.securityRepository.save(authEntity: AuthEntity(token: token))
                    } else {
                        UIApplication.appDelegate()?.rootRouter?.routeToLogin()
                    }

                    strongSelf.requestsToRetry.forEach { $0(succeeded ? RetryResult.retry : RetryResult.doNotRetry) }
                    strongSelf.requestsToRetry.removeAll()
                }
            }
        } else {
            completion(.doNotRetry)
        }
    }
    
    private func refreshToken(completion: @escaping (_ succeeded: Bool, _ token: String?) -> Void) {
        guard !isRefreshing else { return }
        //guard let authData = securityRepository.loadAuthData() else { return }
        
        isRefreshing = true

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            completion(false, nil)
            self.isRefreshing = false
        })
        
        
//        //TODO
//        sessionManager.request(SecurityAPIRouter.refreshToken(dto: RefreshTokenRequestDto(refreshToken: authData.refreshToken)))
//            .responseData { [weak self] (responseData) in
//                guard let strongSelf = self else { return }
//
//                switch responseData.result {
//                case .success:
//                    guard let data = responseData.data else {
//                        completion(false, nil, nil)
//                        return
//                    }
//
//                    do {
//                        let authResponseDto = try JSONDecoder().decode(LoginResponseDto.self, from: data)
//
//                        completion(true, authResponseDto.accessToken, authResponseDto.refreshToken)
//                    } catch {
//                        completion(false, nil, nil)
//                    }
//                case .failure:
//                    completion(false, nil, nil)
//                }
//
//                strongSelf.isRefreshing = false
//        }
    }
}

