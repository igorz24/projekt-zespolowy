//
//  RestServiceFactory.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import Alamofire

final class RestServiceFactory {
    
    private init() {}
    
    static func restService() -> RestServiceProtocol {
        return RestService(session: session(), securityRepository: SecurityFactory.repository())
    }
    
    static func session() -> Session {
        let configuration = URLSessionConfiguration.default
        
        configuration.httpAdditionalHeaders = HTTPHeaders.default.dictionary
                
        var evaluators: [String: ServerTrustEvaluating] = [:]
        evaluators[EnvConfig.ApiHost] = DisabledEvaluator()
        
        let manager = ServerTrustManager(evaluators: evaluators)
        
        
        return Session(configuration: configuration, serverTrustManager: manager)
    }
}

