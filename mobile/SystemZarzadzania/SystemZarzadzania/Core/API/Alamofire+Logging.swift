//
//  Alamofire+Logging.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import Alamofire

extension DataRequest {
    
    @discardableResult
    func log() -> DataRequest {
        cURLDescription(calling: { Logger.log(message: $0) })
        return Logger.logResponse(self, level: EnvConfig.logLevel)
    }
}
