//
//  ErrorModel.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct ErrorModelResponse: Decodable {
    let response: String
}

struct ErrorModel: Decodable {
    let name: String
}

struct ErrorModelError: Decodable {
    let error: String
}

//struct ErrorDetailsModel: Decodable {
//    
//}
