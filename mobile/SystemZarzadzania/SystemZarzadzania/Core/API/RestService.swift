//
//  RestService.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

protocol RestServiceProtocol {
    func getForObject<T: Decodable>(router: BaseAPIRouter) -> Single<T>
    func getForArray<T: Decodable>(router: BaseAPIRouter) -> Single<[T]>
    func getForEmpty(router: BaseAPIRouter) -> Completable
    func getForData(router: BaseAPIRouter) -> Single<Data>
}

class RestService: RestServiceProtocol {

    // MARK: - Properties
    
    let session: Session
    let securityRepository: SecurityRepositoryProtocol
    
    
    // MARK: - Init
    
    init(session: Session, securityRepository: SecurityRepositoryProtocol) {
        self.session = session
        self.securityRepository = securityRepository
    }
    
    
    // MARK: - Methods
    
    func getForObject<T: Decodable>(router: BaseAPIRouter) -> Single<T> {
        return session.request(router, interceptor: setupInterceptor(for: router))
            .validate()
            .log()
            .rx
            .dataResponse()
            .decodeObject(ofType: T.self)
    }
    
    func getForArray<T: Decodable>(router: BaseAPIRouter) -> Single<[T]> {
        return session.request(router, interceptor: setupInterceptor(for: router))
            .validate()
            .log()
            .rx
            .dataResponse()
            .decodeArray(ofType: T.self)
    }
    
    func getForEmpty(router: BaseAPIRouter) -> Completable {
        return session.request(router, interceptor: setupInterceptor(for: router))
            .validate()
            .log()
            .rx
            .dataResponse()
            .asCompletable()
    }
            
    func getForData(router: BaseAPIRouter) -> Single<Data> {
        return session.request(router, interceptor: setupInterceptor(for: router))
            .validate()
            .log()
            .rx
            .dataResponse()
    }
                
    private func setupInterceptor(for router: BaseAPIRouter) -> RequestInterceptor? {
        if router.requiresAuthorization() {
            let handler = OAuth2Handler(securityRepository: securityRepository)
            
            return Interceptor(adapter: handler, retrier: handler)
        } else {
            return nil
        }
    }
}
