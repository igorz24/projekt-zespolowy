//
//  BaseRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import Alamofire

protocol BaseAPIRouter: URLRequestConvertible {
    func baseURLString() -> String
    func method() -> HTTPMethod
    func path() -> String
    func parameters() -> Parameters?
    func encoding() -> ParameterEncoding
    func requiresAuthorization() -> Bool
    func additionalHeaders() -> HTTPHeaders
}

extension BaseAPIRouter {
    
    func baseURLString() -> String {
        return EnvConfig.BaseURL
    }
    
    func method() -> HTTPMethod {
        return .get
    }
    
    func path() -> String {
        return ""
    }
    
    func parameters() -> Parameters? {
        return nil
    }
    
    func encodableData() -> Encodable? {
        return nil
    }
    
    func encoding() -> ParameterEncoding {
        switch method() {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = try URLRequest(url: (baseURLString() + path()).asURL())
        urlRequest.httpMethod = method().rawValue
        
        var headers = HTTPHeaders.default
        for header in additionalHeaders() {
            headers.add(header)
        }
        
        urlRequest.headers = headers
                        
        urlRequest = try encoding().encode(urlRequest, with: parameters())
        
        return urlRequest
    }
    
    func requiresAuthorization() -> Bool {
        return false
    }
    
    func additionalHeaders() -> HTTPHeaders {
        return [:]
    }
    
    public func prepareParameters<T: Encodable>(object: T) -> Parameters {
        guard let jsonData = try? JSONEncoder().encode(object), let dict = (try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)) as? Parameters else {
            return [:]
        }
        
        return dict
    }
}

