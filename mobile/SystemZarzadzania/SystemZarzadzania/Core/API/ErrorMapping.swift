//
//  ErrorMapping.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Alamofire
import Foundation

struct ErrorMapping {
    
    static func mapError(responseData: AFDataResponse<Data>, error: Error) -> Error {
        if let appError = mapResponseError(response: responseData) {
            return appError
        }
                
        print(responseData)
        print(error)
        return AppError(message: "Couldn't parse error")
    }
    
    
    private static func mapResponseError(response: AFDataResponse<Data>) -> Error? {
        guard let errorData = response.data, errorData.count > 0 else {
            return nil
        }
        var errorMessage: String
        do {
            errorMessage = try JSONDecoder().decode(ErrorModel.self, from: errorData).name
        } catch {
            do {
                errorMessage = try JSONDecoder().decode(ErrorModelResponse.self, from: errorData).response
            } catch {
                do {
                    errorMessage = try JSONDecoder().decode(ErrorModelError.self, from: errorData).error
                } catch {
                    return nil
                }
            }
        }
            
        return AppError(message: errorMessage)
    }
}
