//
//  AlertableViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import UIKit

protocol AlertableViewController {
    func showError(error: Error)
}

extension UIViewController: AlertableViewController {
    
    func showError(error: Error) {
        var message: String!

        if let error = error as? AppError {
            message = error.message
        } else {
            message = error.localizedDescription
        }
        
        showAlert(withTitle: "Error!", message: message)
    }
    
    func showAlert(withTitle title: String, message: String, actions: [UIAlertAction] = [UIAlertAction(title: "OK", style: .default, handler: nil)]) {

        let alert = buildAlert(withTitle: title, message: message, actions: actions)

        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
        
    // MARK: - Helper methods

    private func buildAlert(withTitle title: String, message: String, actions: [UIAlertAction] = [UIAlertAction(title: "OK", style: .default, handler: nil)]) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for action in actions {
            alert.addAction(action)
        }
        return alert
    }
    
    
}
