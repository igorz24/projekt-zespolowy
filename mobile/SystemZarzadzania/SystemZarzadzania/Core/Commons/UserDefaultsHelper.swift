//
//  UserDefaultsHelper.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum UserDefaultsKeys: String {
    case role = "role"
}

class UserDefaultsHelper {
    
    
    static func getRole() -> Role {
        let role = UserDefaults.standard.object(forKey: UserDefaultsKeys.role.rawValue) as? String
        return Role(rawValue: role ?? "") ?? .user
    }
    
    static func save(string: String, for key: UserDefaultsKeys) {
        UserDefaults.standard.set(string, forKey: key.rawValue)
    }
}
