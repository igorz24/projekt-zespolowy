//
//  DataFormatter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class CoreDateFormatter {
    
    static func defaultDateString(from date: Date?) -> String? {
        guard let date = date else {
            return nil
        }
        
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        return df.string(from: date)
    }
    
    static func dateAndTimeSlashString(from date: Date?) -> String? {
        guard let date = date else {
            return  nil
        }
        
        let df = DateFormatter()
        df.dateFormat = "HH:mm dd/MM/yyyy"
        return df.string(from: date)
    }
    
    static func defaultDate(from string: String?) -> Date? {
        guard let string = string else {
            return nil
        }
        
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        return df.date(from: string)
    }
    
    static func dateAndTimeSlashDate(from string: String?) -> Date? {
        guard let string = string else {
            return  nil
        }
        
        let df = DateFormatter()
        df.dateFormat = "HH:mm dd/MM/yyyy"
        return df.date(from: string)
    }
    
    static func dateOnlyString(from date: Date?) -> String? {
        guard let date = date else {
            return nil
        }
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        return df.string(from: date)
    }
    
    static func dateAndTimeDateLocale(from string: String?) -> Date? {
        guard let string = string else {
            return nil
        }
        
        let df = DateFormatter()
        df.locale = Locale.current
        df.dateStyle = .short
        df.timeStyle = .short
        
        return df.date(from: string)
    }
    
    static func dateAndTimeStringLocale(from date: Date?) -> String? {
        guard let date = date else {
            return nil
        }
        
        let df = DateFormatter()
        df.locale = Locale.current
        df.dateStyle = .short
        df.timeStyle = .short
        
        return df.string(from: date)
    }
    
    static func dateDateLocale(from string: String?) -> Date? {
        guard let string = string else {
            return nil
        }
        
        let df = DateFormatter()
        df.locale = Locale.current
        df.dateStyle = .short
        
        return df.date(from: string)
    }
    
    static func dateStringLocale(from date: Date?) -> String? {
        guard let date = date else {
            return nil
        }
        
        let df = DateFormatter()
        df.locale = Locale.current
        df.dateStyle = .short
        
        return df.string(from: date)
    }
        
    static func dateAndTime(from string: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        return formatter.date(from: string)
    }
    
    static func dateAndTimeLong(from string: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        return formatter.date(from: string)
    }
    
    static func dateOnly(from string: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        return formatter.date(from: string)
    }
    
    static func dateNoTimeZone(from string: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        return formatter.date(from: string)
    }

    static func dateFromCustomString(decoder: Decoder) throws -> Date {
        let container = try decoder.singleValueContainer()
        let dateString = try container.decode(String.self)
        let length = dateString.count
        var date: Date?
        
        if length > 20 {
            date = dateAndTimeLong(from: dateString)
        } else if length == 20 {
            date = dateAndTime(from: dateString)
        } else if length == 10 {
            date = dateOnly(from: dateString)
        } else if length == 19 {
            date = dateNoTimeZone(from: dateString)
        }
        
        if let date = date {
            return date
        } else {
            throw AppError(message: "Couldn't parse data")
        }
    }
    
    static func timeLeftString(until date: Date) -> String {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .weekOfYear, .day, .hour, .minute], from: Date(), to: date)
        
        if let months = components.month, months > 1 {
            return "\(months) months left!"
        }
        
        if let months = components.month, months == 1 {
            return "\(months) month left!"
        }
        
        if let weeks = components.weekOfYear, weeks > 1 {
            return "\(weeks) weeks left!"
        }
        
        if let weeks = components.weekOfYear, weeks == 1 {
            return "\(weeks) week left!"
        }
        
        if let days = components.day, days > 1 {
            return "\(days) days left!"
        }
        
        if let days = components.day, days == 1 {
            return "\(days) day left!"
        }
        
        if let hours = components.hour, hours > 1 {
            return "\(hours) hours left!"
        }
        
        if let hours = components.hour, hours == 1 {
            return "\(hours) hour left!"
        }
        
        if let minutes = components.minute, minutes > 1 {
            return "\(minutes) minutes left!"
        }
        
        if let minutes = components.minute, minutes == 1 {
            return "\(minutes) minute left!"
        }

        return "Expired"
    }
}

