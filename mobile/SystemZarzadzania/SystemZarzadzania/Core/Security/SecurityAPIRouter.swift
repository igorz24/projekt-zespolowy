//
//  SecurityAPIRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import Alamofire

enum SecurityAPIRouter: BaseAPIRouter {
    case login(dto: LoginRequestDto)
    case register(dto: RegisterRequestDto)
    case sendResetCode(dto: ResetCodeRequestDto)
    case resetPassword(dto: ResetPasswordRequestDto)
    //case refreshToken(dto: RefreshTokenRequestDto)

    func method() -> HTTPMethod {
        switch self {
        case .login, .register, .sendResetCode, .resetPassword:
            return .post
        }
    }
        
    func path() -> String {
        switch self {
        case .login:
            return "/users/signin"
        case .register:
            return "/users/signup"
        case .sendResetCode:
            return "/users/reset/code"
        case .resetPassword:
            return "/users/reset/password"
        }
    }
    
    func parameters() -> Parameters? {
        switch self {
        case .login(let dto):
            return prepareParameters(object: dto)
        case .register(let dto):
            return prepareParameters(object: dto)
        case .resetPassword(let dto):
            return prepareParameters(object: dto)
        case .sendResetCode(let dto):
            return prepareParameters(object: dto)
        default:
            return nil
        }
    }
    
    func encoding() -> ParameterEncoding {
        switch self {
        default:
            return JSONEncoding.default
        }
    }
    
    func requiresAuthorization() -> Bool {
        return false
    }
}

