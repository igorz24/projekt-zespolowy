//
//  SecurityAPIService.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

class SecurityAPIService: SecurityAPIServiceProtocol {
    
    // MARK: - Properties
    
    let restService: RestServiceProtocol
    
    
    // MARK: - Init
    
    init(restService: RestServiceProtocol) {
        self.restService = restService
    }
    
    
    // MARK: - Login
    

    func login(dto: LoginRequestDto) -> Single<LoginResponseDto> {
        return restService.getForObject(router: SecurityAPIRouter.login(dto: dto))
    }
        
    func register(dto: RegisterRequestDto) -> Single<LoginResponseDto> {
        return restService.getForObject(router: SecurityAPIRouter.register(dto: dto))
    }
    
    func sendResetCode(dto: ResetCodeRequestDto) -> Completable {
        return restService.getForEmpty(router: SecurityAPIRouter.sendResetCode(dto: dto))
    }
    
    func resetPassword(dto: ResetPasswordRequestDto) -> Completable {
        return restService.getForEmpty(router: SecurityAPIRouter.resetPassword(dto: dto))
    }
}
