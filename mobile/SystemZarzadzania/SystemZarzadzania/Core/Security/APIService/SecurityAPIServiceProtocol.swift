//
//  SecurityAPIServiceProtocol.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol SecurityAPIServiceProtocol {
    func login(dto: LoginRequestDto) -> Single<LoginResponseDto>
    func register(dto: RegisterRequestDto) -> Single<LoginResponseDto>
    func sendResetCode(dto: ResetCodeRequestDto) -> Completable
    func resetPassword(dto: ResetPasswordRequestDto) -> Completable
}
