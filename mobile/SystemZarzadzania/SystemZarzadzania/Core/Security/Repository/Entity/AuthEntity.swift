//
//  AuthEntity.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import Locksmith

struct AuthEntity: ReadableSecureStorable, CreateableSecureStorable, DeleteableSecureStorable, GenericPasswordSecureStorable {
    
    var token: String = ""
    
    var account: String = SecurityRepositoryKeys.accountName.rawValue
    var service: String = SecurityRepositoryKeys.loginService.rawValue
    
    var data: [String: Any] {
        return [
            SecurityRepositoryKeys.token.rawValue: token,
        ]
    }

    init(token: String = "") {
        self.token = token

    }
    
    init(from dict: [String: Any]) {
        guard let token = dict[SecurityRepositoryKeys.token.rawValue] as? String else {
                return
        }
        
        self.token = token
    }
}

