//
//  SecurityRepository.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import Locksmith
import RealmSwift

class SecurityRepository: SecurityRepositoryProtocol {
    
    
    // MARK: - Auth
    
    func save(authEntity: AuthEntity) throws {
        do {
            try authEntity.updateInSecureStore()
        } catch {
            throw AppError(error: error)
        }
    }
    
    func loadAuthData() -> AuthEntity? {
        guard let data = AuthEntity().readFromSecureStore()?.data else {
            return nil
        }
        
        return AuthEntity(from: data)
    }
        
    func isAuthorized() -> Bool {
        guard let authData = loadAuthData() else {
            return false
        }
        
        return !authData.token.isEmpty
    }
    
    func clearAuthorization() {
        try? AuthEntity().deleteFromSecureStore()
    }
}
