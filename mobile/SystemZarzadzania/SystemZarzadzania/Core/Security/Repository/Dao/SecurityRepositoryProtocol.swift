//
//  SecurityRepositoryProtocol.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol SecurityRepositoryProtocol {
    func save(authEntity: AuthEntity) throws
    func loadAuthData() -> AuthEntity?
    func isAuthorized() -> Bool
    func clearAuthorization()
}

enum SecurityRepositoryKeys: String {
    case token = "token"
    case accountName = "managment"
    case loginService = "login"
}
