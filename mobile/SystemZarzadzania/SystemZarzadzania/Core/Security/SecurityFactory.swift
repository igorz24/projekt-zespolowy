//
//  SecurityFactory.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

final class SecurityFactory {
    
    // MARK: - Init
    
    private init() {}
    
    
    // MARK: - Methods
    
    static func apiService() -> SecurityAPIServiceProtocol {
        return SecurityAPIService(restService: RestServiceFactory.restService())
    }

    static func repository() -> SecurityRepositoryProtocol {
        return SecurityRepository()
    }
}

