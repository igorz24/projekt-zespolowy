//
//  LoginResponseDto.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import UIKit

enum Role: String {
    case admin = "ADMIN"
    case manager = "MANAGER"
    case user = "USER"
    
    var viewControllers: [UIViewController] {
        switch self {
        case .user:
            let vc = UIStoryboard.viewController(for: .commit)
            let tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
            vc.tabBarItem = tabBarItem
            let vc2 = UIStoryboard.viewController(for: .task)
            let tabBarItem2 = UITabBarItem(tabBarSystemItem: .history, tag: 1)
            vc2.tabBarItem = tabBarItem2
            let vcOther = UIStoryboard.viewController(for: .more)
            let tabBarIteme = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 2)
            vcOther.tabBarItem = tabBarIteme
            return [vc, vc2, vcOther]
        case .manager:
            let vc = UIStoryboard.viewController(for: .commit)
            let tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
            vc.tabBarItem = tabBarItem
            let vc2 = UIStoryboard.viewController(for: .task)
            let tabBarItem2 = UITabBarItem(tabBarSystemItem: .history, tag: 1)
            vc2.tabBarItem = tabBarItem2
            
            let vc3 = UIStoryboard.viewController(for: .project)
            let tabBarItem3 = UITabBarItem(tabBarSystemItem: .favorites, tag: 2)
            vc3.tabBarItem = tabBarItem3
            
            let vcOther = UIStoryboard.viewController(for: .more)
            let tabBarItem4 = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 3)
            vcOther.tabBarItem = tabBarItem4
            return [vc, vc2, vc3, vcOther]
        case .admin:
            let vc = UIStoryboard.viewController(for: .commit)
            let tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
            vc.tabBarItem = tabBarItem
            let vc2 = UIStoryboard.viewController(for: .task)
            let tabBarItem2 = UITabBarItem(tabBarSystemItem: .history, tag: 1)
            vc2.tabBarItem = tabBarItem2
            
            let vc3 = UIStoryboard.viewController(for: .project)
            let tabBarItem3 = UITabBarItem(tabBarSystemItem: .favorites, tag: 2)
            vc3.tabBarItem = tabBarItem3
            
            let vc4 = UIStoryboard.viewController(for: .user)
            let tabBarItem4 = UITabBarItem(tabBarSystemItem: .featured, tag: 3)
            vc4.tabBarItem = tabBarItem4
            
            
            let vcOther = UIStoryboard.viewController(for: .more)
            let tabBarItem5 = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 3)
            vcOther.tabBarItem = tabBarItem5
            return [vc, vc2, vc3, vc4, vcOther]
        }
    }
    
    var additionalOptions: [More.Option] {
        switch self {
        case .user:
            return [.logout]
        case .manager:
            return [.logout]
        case .admin:
            return [.logout]
        }
    }
}

struct LoginResponseDto: Decodable {
    let token: String
    let profile: Profile
}

struct Profile: Decodable {
    let id: Int
    let email: String
    let role: Role
    let firstName: String
    let lastName: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case email = "email"
        case role = "role"
        case firstName = "firstName"
        case lastName = "lastName"
    }
    
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(Int.self, forKey: .id)
        email = try values.decode(String.self, forKey: .email)
        let roleString = try values.decode(String.self, forKey: .role)
        role = Role(rawValue: roleString) ?? .user
        firstName = try values.decode(String.self, forKey: .firstName)
        lastName = try values.decode(String.self, forKey: .lastName)
    }
}
