//
//  LoginRequest.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct LoginRequestDto: Encodable {
    let email: String
    let password: String
}
