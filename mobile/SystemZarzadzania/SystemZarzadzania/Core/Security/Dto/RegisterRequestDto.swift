//
//  RegisterRequestDto.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct RegisterRequestDto: Encodable {
    let email: String
    let password: String
    let firstName: String
    let lastName: String
}
