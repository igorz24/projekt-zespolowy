//
//  ResetPasswordDto.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import Foundation

struct ResetPasswordRequestDto: Encodable {
    let password: String
    let resetCode: String
}
