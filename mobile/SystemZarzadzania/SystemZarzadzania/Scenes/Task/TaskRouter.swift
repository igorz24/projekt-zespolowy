//
//  TaskRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 27/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

@objc protocol TaskRoutingLogic {
    func routeToModifyTask(taskId: Int)
    func routeToAddTask()
}

protocol TaskDataPassing {
    var dataStore: TaskDataStore? { get }
}

class TaskRouter: NSObject, TaskRoutingLogic, TaskDataPassing {
    
    // MARK: - Properties
    
    weak var viewController: TaskViewController?
    var dataStore: TaskDataStore?
    
    
    // MARK: - Initializer

    init(viewController: TaskViewController?, dataStore: TaskDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }

    
    // MARK: - Methods
    
    
    func routeToAddTask() {
        viewController?.navigationController?.pushViewController(UIStoryboard.viewController(for: .addTask), animated: true)
    }
    
    func routeToModifyTask(taskId: Int) {
        guard let vc = UIStoryboard.viewController(for: .addTask) as? AddTaskViewController , var store = vc.router?.dataStore else { return }
        vc.purpose = .modify
        passData(taskId: taskId, dataStore: &store)
        
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func passData(taskId: Int, dataStore: inout AddTaskDataStore) {
        dataStore.taskId = taskId
    }
}
