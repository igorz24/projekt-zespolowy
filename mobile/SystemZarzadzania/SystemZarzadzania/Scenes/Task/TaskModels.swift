//
//  TaskModels.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 27/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum Task {
    
    // MARK: - Use cases
    
    enum GetTasks {
        struct Request {
            let searchString: String?
        }
        
        enum Response {
            case success([TaskResponseDto])
            case error(Error)
        }
        
        struct ViewModel {
            let id: Int
            let projectId: Int
            let managerId: Int
            let description: String
            let estimationTime: Int
        }
    }
    
    enum Delete {
        struct Request {
            let completionHandler: (Bool) -> Void
            let taskId: Int
        }
        
        enum Response {
            case success((Bool) -> Void)
            case error(Error, (Bool) -> Void)
        }
    }
}
