//
//  TaskConfigurator.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 27/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class TaskConfigurator {
    
    // MARK: - Configuration
    
    func configure(viewController: TaskViewController) {
        let presenter = TaskPresenter(viewController: viewController)
        let worker = TaskWorker(customerAPIService: CustomerFactory.apiService())
        let interactor = TaskInteractor(presenter: presenter, worker: worker)
        let router = TaskRouter(viewController: viewController, dataStore: interactor)

        viewController.interactor = interactor
        viewController.router = router
    }
}
