//
//  TaskTableViewCell.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    
    @IBOutlet weak var projectId: UILabel!
    @IBOutlet weak var managerId: UILabel!
    @IBOutlet weak var estimatedTime: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var taskId: UILabel!
    
    static let cellIdentifier = "TaskTableViewCellIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(data: Task.GetTasks.ViewModel) {
        projectId.text = String(data.projectId)
        managerId.text = String(data.managerId)
        estimatedTime.text = String(data.estimationTime)
        descriptionLabel.text = data.description
        taskId.text = String(data.id)
    }

}
