//
//  TaskWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 27/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol TaskWorkerLogic {
    func deleteTask(taskId: Int) -> Completable
    func getTasks(request: Task.GetTasks.Request) -> Single<[TaskResponseDto]>
}

class TaskWorker: TaskWorkerLogic {
    
    let customerAPIService: CustomerAPIServiceProtocol
    
    // MARK: - Init
    
    init(customerAPIService: CustomerAPIServiceProtocol) {
        self.customerAPIService = customerAPIService
    }
    

    // MARK: - Methods
    
    func deleteTask(taskId: Int) -> Completable {
        return customerAPIService.deleteTask(dto: DeleteTaskRequestDto(taskId: taskId))
    }
    
    func getTasks(request: Task.GetTasks.Request) -> Single<[TaskResponseDto]> {
        let role = UserDefaultsHelper.getRole()
        if role == .admin {
            return customerAPIService.getAllTask(dto: TaskRequestDto(textSearch: request.searchString))
        } else {
            return customerAPIService.getTask(dto: TaskRequestDto(textSearch: request.searchString))
        }
    }
}
