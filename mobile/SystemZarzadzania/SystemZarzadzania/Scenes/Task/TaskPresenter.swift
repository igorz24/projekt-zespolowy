//
//  TaskPresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 27/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol TaskPresentationLogic {
    func presentGetTasks(response: Task.GetTasks.Response)
    func presentDeleteTask(response: Task.Delete.Response)
}

class TaskPresenter: TaskPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: TaskDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: TaskDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods
    
func presentDeleteTask(response: Task.Delete.Response) {
    switch response {
    case .success(let handler):
        viewController?.deleteSuccess(handler: handler)
    case .error(let error, let handler):
        viewController?.deleteError(message: error.localizedDescription, handler: handler)
        
    }
}

func presentGetTasks(response: Task.GetTasks.Response) {
    switch response {
    case .success(let data):
        
        let viewModel = data.map { Task.GetTasks.ViewModel(id: $0._id, projectId: $0.projectId, managerId: $0.managerId, description: $0.description, estimationTime: $0.estimationTime) }
        
       viewController?.displayGetTasks(viewModel: viewModel)
    case .error(let error):
        viewController?.showError(error: error)
    }
}
}
