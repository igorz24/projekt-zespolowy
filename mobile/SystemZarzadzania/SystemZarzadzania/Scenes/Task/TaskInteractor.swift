//
//  TaskInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 27/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol TaskBusinessLogic {
    func getTasks(request: Task.GetTasks.Request)
    func deleteTask(request: Task.Delete.Request)
}

protocol TaskDataStore {
    var options: [TaskDataStoreOptionsKey : Any] { get set }
}

enum TaskDataStoreOptionsKey: String {
    case some
}

class TaskInteractor: TaskBusinessLogic, TaskDataStore {
    
    // MARK: - Properties
    
    var presenter: TaskPresentationLogic?
    let worker: TaskWorkerLogic
    var options: [TaskDataStoreOptionsKey : Any] = [:]
    let disposeBag = DisposeBag()

    
    // MARK: - Initializer

    init(presenter: TaskPresentationLogic?, worker: TaskWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
    func getTasks(request: Task.GetTasks.Request) {
        worker.getTasks(request: request)
        .subscribe(onSuccess: { data in
            self.presenter?.presentGetTasks(response: .success(data))
        }, onError: { error in
            self.presenter?.presentGetTasks(response: .error(error))
        })
        .disposed(by: disposeBag)
    }

    func deleteTask(request: Task.Delete.Request) {
        worker.deleteTask(taskId: request.taskId)
        .subscribe(onCompleted: {
            self.presenter?.presentDeleteTask(response: .success(request.completionHandler))
        }, onError: { error in
            self.presenter?.presentDeleteTask(response: .error(error, request.completionHandler))
            })
        .disposed(by: disposeBag)
        
        }
}
