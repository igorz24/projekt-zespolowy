//
//  TaskViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 27/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

protocol TaskDisplayLogic: AnyObject {
    func deleteSuccess(handler: (Bool) -> Void)
    func deleteError(message: String, handler: (Bool) -> Void)
    func showError(error: Error)
    func displayGetTasks(viewModel: [Task.GetTasks.ViewModel])
}

class TaskViewController: UIViewController {
    
    @IBOutlet weak var addNewTaskButton: UIButton!
    @IBOutlet weak var filterTextField: UITextField!
    
    
    @IBAction func FilterAction(_ sender: Any) {
        interactor?.getTasks(request: Task.GetTasks.Request(searchString: filterTextField.text))
    }
    
    @IBAction func addNewAction(_ sender: Any) {
        router?.routeToAddTask()
    }
    
    // MARK: - Properties
    
    var interactor: TaskBusinessLogic?
    var router: (NSObjectProtocol & TaskRoutingLogic & TaskDataPassing)?
    var data: [Task.GetTasks.ViewModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        TaskConfigurator().configure(viewController: self)
    }

    
    // MARK: - Routing
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let scene = segue.identifier {
//            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
//            if let router = router, router.responds(to: selector) {
//                router.perform(selector, with: segue)
//            }
//        }
//    }
    
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    let role = UserDefaultsHelper.getRole()
    if role == .user {
        addNewTaskButton.isHidden = true
    }
        
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 100.0
    interactor?.getTasks(request: Task.GetTasks.Request(searchString: nil))
    }
}

extension TaskViewController: TaskDisplayLogic {
    func deleteSuccess(handler: (Bool) -> Void) {
        handler(true)
    }
    
    func deleteError(message: String, handler: (Bool) -> Void) {
        handler(false)
        showError(error: AppError(message: message))
    }
    
    func displayGetTasks(viewModel: [Task.GetTasks.ViewModel]) {
        data = viewModel
        tableView.reloadData()
    }
}

extension TaskViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TaskTableViewCell.cellIdentifier) as? TaskTableViewCell else {
            fatalError()
        }
        
        cell.setup(data: data[indexPath.row])
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    
}

extension TaskViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let role = UserDefaultsHelper.getRole()
        
        if role == .manager {
            let taskId = data[indexPath.row].id
            let deleteAction = UIContextualAction(style: .destructive, title: "Usuń") { (action, view, completionHandler) in
                self.interactor?.deleteTask(request: Task.Delete.Request(completionHandler: completionHandler, taskId: taskId))
            }
            
            let modifyAction = UIContextualAction(style: .normal, title: "Modyfikuj") { (action, view, completionHandler) in
                self.router?.routeToModifyTask(taskId: taskId)
                completionHandler(true)
            }
            
            let config = UISwipeActionsConfiguration(actions: [modifyAction, deleteAction])
            return config
        } else {
            return nil
        }
    }
}
