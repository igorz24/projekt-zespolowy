//
//  TaskTableViewCell.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell {

    @IBOutlet weak var managerIdLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var projectIdLabel: UILabel!
    
    static let cellIdentifier = "ProjectTableViewCellIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(data: Project.GetProjects.ViewModel) {
        if data.managerId != nil {
            managerIdLabel.text = String(data.managerId!)
        } else {
            managerIdLabel.text = "Brak"
        }
        descriptionLabel.text = data.description
        nameLabel.text = data.name
        projectIdLabel.text = String(data.projectId)
    }

}
