//
//  ProjectWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol ProjectWorkerLogic {
    func deleteProject(projectId: Int) -> Completable
    func getMyProjects(request: Project.GetProjects.Request) -> Single<[ProjectResponseDto]>
    func getProjects(request: Project.GetProjects.Request) -> Single<[ProjectResponseDto]>
}

class ProjectWorker: ProjectWorkerLogic {
    
    // MARK: - Properties
    
    let customerAPIService: CustomerAPIServiceProtocol
    
    // MARK: - Init
    
    init(customerAPIService: CustomerAPIServiceProtocol) {
        self.customerAPIService = customerAPIService
    }
    

    // MARK: - Methods
    
    func deleteProject(projectId: Int) -> Completable {
        return customerAPIService.deleteProject(dto: DeleteProjectRequestDto(projectId: projectId))
    }
    
    func getMyProjects(request: Project.GetProjects.Request) -> Single<[ProjectResponseDto]> {
        return customerAPIService.getMyProjects(dto: ProjectRequestDto(textSearch: request.searchString))
    }
    
    func getProjects(request: Project.GetProjects.Request) -> Single<[ProjectResponseDto]> {
        return customerAPIService.getProjects(dto: ProjectRequestDto(textSearch: request.searchString))
    }
}
