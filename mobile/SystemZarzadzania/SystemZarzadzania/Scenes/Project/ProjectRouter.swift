//
//  ProjectRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

protocol ProjectRoutingLogic {
    func routeToModifyProject(projectId: Int)
    func routeToAddProject()
    func routeToProjectDetails(viewModel: Project.GetProjects.ViewModel)
}

protocol ProjectDataPassing {
    var dataStore: ProjectDataStore? { get }
}

class ProjectRouter: NSObject, ProjectRoutingLogic, ProjectDataPassing {
    
    // MARK: - Properties
    
    weak var viewController: ProjectViewController?
    var dataStore: ProjectDataStore?
    
    
    // MARK: - Initializer

    init(viewController: ProjectViewController?, dataStore: ProjectDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }

    
    // MARK: - Methods
    
    func routeToAddProject() {
        guard let vc = UIStoryboard.viewController(for: .addProject) as? AddProjectViewController else { return }
        vc.purpose = .add
        
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }

    func routeToModifyProject(projectId: Int) {
        guard let vc = UIStoryboard.viewController(for: .addProject) as? AddProjectViewController , var store = vc.router?.dataStore else { return }
        vc.purpose = .modify
        passData(projectId: projectId, dataStore: &store)
        
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func routeToProjectDetails(viewModel: Project.GetProjects.ViewModel) {
        guard let vc = UIStoryboard.viewController(for: .projectDetails) as? ProjectDetailsViewController , var store = vc.router?.dataStore else { return }

        passDataToProjectDetails(viewModel: viewModel, dataStore: &store)
        
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }

    func passData(projectId: Int, dataStore: inout AddProjectDataStore) {
        dataStore.projectId = projectId
    }
    
    func passDataToProjectDetails(viewModel: Project.GetProjects.ViewModel, dataStore: inout ProjectDetailsDataStore) {
        dataStore.viewModel = viewModel
    }
}
