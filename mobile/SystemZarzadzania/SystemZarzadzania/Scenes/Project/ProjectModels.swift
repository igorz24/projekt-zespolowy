//
//  ProjectModels.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum Project {
    
    // MARK: - Use cases
    
    enum GetProjects {
        struct Request {
            let searchString: String?
        }
        
        enum Response {
            case success([ProjectResponseDto])
            case error(Error)
        }
        
        struct ViewModel {
            let projectId: Int
            let managerId: Int?
            let description: String
            let name: String
            let developers: [Developer]
            
            struct Developer {
                let id: Int
                let firstName: String
                let lastName: String
            }
        }
    }
    
    enum Delete {
        struct Request {
            let completionHandler: (Bool) -> Void
            let projectId: Int
        }
        
        enum Response {
            case success((Bool) -> Void)
            case error(Error, (Bool) -> Void)
        }
    }
}
