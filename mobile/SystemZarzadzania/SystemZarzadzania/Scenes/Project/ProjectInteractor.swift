//
//  ProjectInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol ProjectBusinessLogic {
    func getProjects(request: Project.GetProjects.Request)
    func getAllProject(request: Project.GetProjects.Request)
    func deleteProject(request: Project.Delete.Request)
}

protocol ProjectDataStore {
    var options: [ProjectDataStoreOptionsKey : Any] { get set }
}

enum ProjectDataStoreOptionsKey: String {
    case some
}

class ProjectInteractor: ProjectBusinessLogic, ProjectDataStore {
    
    // MARK: - Properties
    
    var presenter: ProjectPresentationLogic?
    let worker: ProjectWorkerLogic
    var options: [ProjectDataStoreOptionsKey : Any] = [:]
    let disposeBag = DisposeBag()

    
    // MARK: - Initializer

    init(presenter: ProjectPresentationLogic?, worker: ProjectWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
    func getProjects(request: Project.GetProjects.Request) {
        worker.getMyProjects(request: request)
        .subscribe(onSuccess: { data in
            self.presenter?.presentGetProjects(response: .success(data))
        }, onError: { error in
            self.presenter?.presentGetProjects(response: .error(error))
        })
        .disposed(by: disposeBag)
    }
    
    func getAllProject(request: Project.GetProjects.Request) {
        worker.getProjects(request: request)
        .subscribe(onSuccess: { data in
            self.presenter?.presentGetProjects(response: .success(data))
        }, onError: { error in
            self.presenter?.presentGetProjects(response: .error(error))
        })
        .disposed(by: disposeBag)
    }

    func deleteProject(request: Project.Delete.Request) {
        worker.deleteProject(projectId: request.projectId)
        .subscribe(onCompleted: {
            self.presenter?.presentDeleteProject(response: .success(request.completionHandler))
        }, onError: { error in
            self.presenter?.presentDeleteProject(response: .error(error, request.completionHandler))
            })
        .disposed(by: disposeBag)
        
        }
}
