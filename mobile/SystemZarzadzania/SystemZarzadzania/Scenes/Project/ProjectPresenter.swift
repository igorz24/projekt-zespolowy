//
//  ProjectPresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol ProjectPresentationLogic {
    func presentDeleteProject(response: Project.Delete.Response)
    func presentGetProjects(response: Project.GetProjects.Response)
}

class ProjectPresenter: ProjectPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: ProjectDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: ProjectDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods
    
    func presentDeleteProject(response: Project.Delete.Response) {
        switch response {
        case .success(let handler):
            viewController?.deleteSuccess(handler: handler)
        case .error(let error, let handler):
            viewController?.deleteError(message: error.localizedDescription, handler: handler)
            
        }
    }

    func presentGetProjects(response: Project.GetProjects.Response) {
        switch response {
        case .success(let data):
            
            let viewModel = data.map { Project.GetProjects.ViewModel(projectId: $0._id, managerId: $0.manager?.id, description: $0.description, name: $0.name, developers: $0.developers.map { developer in Project.GetProjects.ViewModel.Developer(id: developer.id, firstName: developer.firstName, lastName: developer.lastName) })}
            
           viewController?.displayGetProjects(viewModel: viewModel)
        case .error(let error):
            viewController?.showError(error: error)
        }
    }
}
