//
//  ProjectConfigurator.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class ProjectConfigurator {
    
    // MARK: - Configuration
    
    func configure(viewController: ProjectViewController) {
        let presenter = ProjectPresenter(viewController: viewController)
        let worker = ProjectWorker(customerAPIService: CustomerFactory.apiService())
        let interactor = ProjectInteractor(presenter: presenter, worker: worker)
        let router = ProjectRouter(viewController: viewController, dataStore: interactor)

        viewController.interactor = interactor
        viewController.router = router
    }
}
