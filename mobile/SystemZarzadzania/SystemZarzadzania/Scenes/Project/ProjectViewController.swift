//
//  ProjectViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

protocol ProjectDisplayLogic: AnyObject {
    func deleteSuccess(handler: (Bool) -> Void)
    func deleteError(message: String, handler: (Bool) -> Void)
    func showError(error: Error)
    func displayGetProjects(viewModel: [Project.GetProjects.ViewModel])
}

class ProjectViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterTextField: UITextField!
    
    
    // MARK: - IBActions
    
    @IBAction func filterAction(_ sender: Any) {
        let role = UserDefaultsHelper.getRole()
        
        if role == .admin {
            interactor?.getAllProject(request: Project.GetProjects.Request(searchString: filterTextField.text))
        } else {
            interactor?.getProjects(request: Project.GetProjects.Request(searchString: filterTextField.text))
        }
    }
    
    @IBAction func addNewProjectAction(_ sender: Any) {
        router?.routeToAddProject()
    }
    
    
    // MARK: - Properties
    
    var interactor: ProjectBusinessLogic?
    var router: (NSObjectProtocol & ProjectRoutingLogic & ProjectDataPassing)?
    var data: [Project.GetProjects.ViewModel] = []
    
    
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ProjectConfigurator().configure(viewController: self)
    }
    
    
    // MARK: - Routing
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //        if let scene = segue.identifier {
    //            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
    //            if let router = router, router.responds(to: selector) {
    //                router.perform(selector, with: segue)
    //            }
    //        }
    //    }
    
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100.0
        
        let role = UserDefaultsHelper.getRole()
        
        if role == .admin {
            interactor?.getAllProject(request: Project.GetProjects.Request(searchString: nil))
        } else {
            interactor?.getProjects(request: Project.GetProjects.Request(searchString: nil))
        }
    }
}

extension ProjectViewController: ProjectDisplayLogic {
    func deleteSuccess(handler: (Bool) -> Void) {
        handler(true)
    }
    
    func deleteError(message: String, handler: (Bool) -> Void) {
        handler(false)
        showError(error: AppError(message: message))
    }
    
    func displayGetProjects(viewModel: [Project.GetProjects.ViewModel]) {
        data = viewModel
        tableView.reloadData()
    }
}

extension ProjectViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProjectTableViewCell.cellIdentifier) as? ProjectTableViewCell else {
            fatalError()
        }
        
        cell.setup(data: data[indexPath.row])
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
}

extension ProjectViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let role = UserDefaultsHelper.getRole()
        
        if role == .admin {
            let projectId = data[indexPath.row].projectId
            let deleteAction = UIContextualAction(style: .destructive, title: "Usuń") { (action, view, completionHandler) in
            self.interactor?.deleteProject(request: Project.Delete.Request(completionHandler: completionHandler, projectId: projectId))
            }
        
            let modifyAction = UIContextualAction(style: .normal, title: "Modyfikuj") { (action, view,  completionHandler) in
                self.router?.routeToModifyProject(projectId: projectId)
                completionHandler(true)
            }
        
            let config = UISwipeActionsConfiguration(actions: [modifyAction, deleteAction])
            return config
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router?.routeToProjectDetails(viewModel: data[indexPath.row])
    }
}
