//
//  RegisterInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol RegisterBusinessLogic {
    func register(request: Register.Register.Request)
}

protocol RegisterDataStore {
    var options: [RegisterDataStoreOptionsKey : Any] { get set }
}

enum RegisterDataStoreOptionsKey: String {
    case some
}

class RegisterInteractor: RegisterBusinessLogic, RegisterDataStore {
    
    // MARK: - Properties
    
    var presenter: RegisterPresentationLogic?
    let worker: RegisterWorkerLogic
    var options: [RegisterDataStoreOptionsKey : Any] = [:]
    var disposeBag = DisposeBag()

    
    // MARK: - Initializer

    init(presenter: RegisterPresentationLogic?, worker: RegisterWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
    func register(request: Register.Register.Request) {
        worker.register(request: request)
        .subscribe(onSuccess: { [weak self] _ in
            self?.presenter?.presentRegister(response: .success)
            }, onError: { [weak self] error in
                self?.presenter?.presentRegister(response: .error(error))
        })
        .disposed(by: disposeBag)
    }
}
