//
//  RegisterViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol RegisterDisplayLogic: AnyObject {
    func displayRegisterSuccess()
    func showError(error: Error)
}

class RegisterViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    // MARK: - IBActions
    
    @IBAction func registerAction(_ sender: Any) {
        interactor?.register(request: Register.Register.Request(email: email.text!, password: passwordTextField.text!, name: nameTextField.text!, lastName: lastNameTextField.text!))
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Properties
    
    var interactor: RegisterBusinessLogic?
    var router: (NSObjectProtocol & RegisterRoutingLogic & RegisterDataPassing)?
    var disposeBag = DisposeBag()
    
        
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        RegisterConfigurator().configure(viewController: self)
    }
   
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRx()
    }
    
    func setupRx() {
        let emailValid = email.rx.text.orEmpty.map { $0.count > 0 }
        let passwordValid = passwordTextField.rx.text.orEmpty.map { $0.count > 0 }
        let nameValid = nameTextField.rx.text.orEmpty.map { $0.count > 0 }
        let lastNameValid = lastNameTextField.rx.text.orEmpty.map { $0.count > 0 }
        
        Observable.combineLatest(emailValid, passwordValid, nameValid, lastNameValid) { $0 && $1 && $2 && $3 }
            .bind(to: registerButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }
}

extension RegisterViewController: RegisterDisplayLogic {

    func displayRegisterSuccess() {
        router?.routeToMain()
    }
}
