//
//  RegisterWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol RegisterWorkerLogic {
    func register(request: Register.Register.Request) -> Single<LoginResponseDto>
}

class RegisterWorker: RegisterWorkerLogic {
    
    // MARK: - Properties
    
    let securityAPIService: SecurityAPIServiceProtocol
    let securityRepository: SecurityRepositoryProtocol
    
    // MARK: - Init
    
    init(securityAPIService: SecurityAPIServiceProtocol, securityRepository: SecurityRepositoryProtocol) {
        self.securityAPIService = securityAPIService
        self.securityRepository = securityRepository
    }
    

    // MARK: - Methods
    
    func register(request: Register.Register.Request) -> Single<LoginResponseDto> {
        securityAPIService.register(dto: RegisterRequestDto(email: request.email, password: request.password, firstName: request.name, lastName: request.lastName))
            .do(onSuccess: { [weak self] response in
                try? self?.securityRepository.save(authEntity: AuthEntity(token: response.token))
                UserDefaultsHelper.save(string: response.profile.role.rawValue, for: .role)
            })
    }
}
