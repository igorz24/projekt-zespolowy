//
//  RegisterConfigurator.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class RegisterConfigurator {
    
    // MARK: - Configuration
    
    func configure(viewController: RegisterViewController) {
        let presenter = RegisterPresenter(viewController: viewController)
        let worker = RegisterWorker(securityAPIService: SecurityFactory.apiService(), securityRepository: SecurityFactory.repository())
        let interactor = RegisterInteractor(presenter: presenter, worker: worker)
        let router = RegisterRouter(viewController: viewController, dataStore: interactor)

        viewController.interactor = interactor
        viewController.router = router
    }
}
