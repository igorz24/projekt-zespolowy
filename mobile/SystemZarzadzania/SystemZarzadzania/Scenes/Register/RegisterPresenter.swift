//
//  RegisterPresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol RegisterPresentationLogic {
    func presentRegister(response: Register.Register.Response)
}

class RegisterPresenter: RegisterPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: RegisterDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: RegisterDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods
    
    func presentRegister(response: Register.Register.Response) {
        switch response {
        case .success:
            viewController?.displayRegisterSuccess()
        case .error(let error):
            viewController?.showError(error: error)
        }
    }
}
