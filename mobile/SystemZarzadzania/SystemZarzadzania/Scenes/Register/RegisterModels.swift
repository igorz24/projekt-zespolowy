//
//  RegisterModels.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum Register {
    
    // MARK: - Use cases
    
    enum Register {
        struct Request {
            let email: String
            let password: String
            let name: String
            let lastName: String
        }
        
        enum Response {
            case success
            case error(Error)
        }
    }
}
