//
//  RegisterRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

@objc protocol RegisterRoutingLogic {
    func routeToMain()
}

protocol RegisterDataPassing {
    var dataStore: RegisterDataStore? { get }
}

class RegisterRouter: NSObject, RegisterRoutingLogic, RegisterDataPassing {
    
    // MARK: - Properties
    
    weak var viewController: RegisterViewController?
    var dataStore: RegisterDataStore?
    
    
    // MARK: - Initializer

    init(viewController: RegisterViewController?, dataStore: RegisterDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }

    
    // MARK: - Methods
    
    func routeToMain() {
        viewController?.navigationController?.popViewController(animated: true)
    }
    
}
