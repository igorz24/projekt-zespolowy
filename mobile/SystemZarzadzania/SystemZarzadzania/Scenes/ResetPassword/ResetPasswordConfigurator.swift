//
//  ResetPasswordConfigurator.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class ResetPasswordConfigurator {
    
    // MARK: - Configuration
    
    func configure(viewController: ResetPasswordViewController) {
        let presenter = ResetPasswordPresenter(viewController: viewController)
        let worker = ResetPasswordWorker(securityApiService: SecurityFactory.apiService())
        let interactor = ResetPasswordInteractor(presenter: presenter, worker: worker)
        let router = ResetPasswordRouter(viewController: viewController, dataStore: interactor)

        viewController.interactor = interactor
        viewController.router = router
    }
}
