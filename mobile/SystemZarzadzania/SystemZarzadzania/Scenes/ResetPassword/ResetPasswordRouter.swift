//
//  ResetPasswordRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

@objc protocol ResetPasswordRoutingLogic {
    //func routeToSomewhere(segue: UIStoryboardSegue?)
}

protocol ResetPasswordDataPassing {
    var dataStore: ResetPasswordDataStore? { get }
}

class ResetPasswordRouter: NSObject, ResetPasswordRoutingLogic, ResetPasswordDataPassing {
    
    // MARK: - Properties
    
    weak var viewController: ResetPasswordViewController?
    var dataStore: ResetPasswordDataStore?
    
    
    // MARK: - Initializer

    init(viewController: ResetPasswordViewController?, dataStore: ResetPasswordDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }

    
    // MARK: - Methods
    
//    func routeToSomewhere(segue: UIStoryboardSegue?) {
//        if let segue = segue {
//            let destinationVC = segue.destination as! SomewhereViewController
//            var destinationDS = destinationVC.router!.dataStore!
//            passDataToSomewhere(source: dataStore!, destination: &destinationDS)
//        } else {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let destinationVC = storyboard.instantiateViewController(withIdentifier: "SomewhereViewController") as! SomewhereViewController
//            var destinationDS = destinationVC.router!.dataStore!
//            passDataToSomewhere(source: dataStore!, destination: &destinationDS)
//            navigateToSomewhere(source: viewController!, destination: destinationVC)
//        }
//    }
    
    
    // MARK: - Navigation
    
//    func navigateToSomewhere(source: ResetPasswordViewController, destination: SomewhereViewController) {
//        source.show(destination, sender: nil)
//    }
    
    
    // MARK: - Passing data
    
//    func passDataToSomewhere(source: ResetPasswordDataStore, destination: inout SomewhereDataStore) {
//        destination.name = source.name
//    }
}
