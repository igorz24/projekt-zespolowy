//
//  ResetPasswordModels.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum ResetPassword {
    
    // MARK: - Use cases
    
    enum ChangePassword {
        struct Request {}
        
        enum Response {
            case success
            case error(Error)
        }
        
        struct ViewModel {}
    }
}
