//
//  ResetPasswordPresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol ResetPasswordPresentationLogic {
    func presentChangePassword(response: ResetPassword.ChangePassword.Response)
}

class ResetPasswordPresenter: ResetPasswordPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: ResetPasswordDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: ResetPasswordDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods
    
    func presentChangePassword(response: ResetPassword.ChangePassword.Response) {
        switch response {
        case .success:
            viewController?.showSuccess()
        case .error(let error):
            viewController?.showError(error: error)
        }
    }
}
