//
//  ResetPasswordViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

protocol ResetPasswordDisplayLogic: AnyObject {
    func showError(error: Error)
    func showSuccess()
}

class ResetPasswordViewController: UIViewController {
    
    @IBOutlet weak var newPasswordTextField: UITextField!
    
    @IBAction func changePasswordActiion(_ sender: Any) {
        interactor?.resetPassword(password: newPasswordTextField.text ?? "")
    }
    // MARK: - Properties
    
    var interactor: ResetPasswordBusinessLogic?
    var router: (NSObjectProtocol & ResetPasswordRoutingLogic & ResetPasswordDataPassing)?
    
        
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ResetPasswordConfigurator().configure(viewController: self)
    }

    
    // MARK: - Routing
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let scene = segue.identifier {
//            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
//            if let router = router, router.responds(to: selector) {
//                router.perform(selector, with: segue)
//            }
//        }
//    }
    
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        doSomething()
    }
}

extension ResetPasswordViewController: ResetPasswordDisplayLogic {
    func showSuccess() {
        navigationController?.popViewController(animated: true)
    }
}
