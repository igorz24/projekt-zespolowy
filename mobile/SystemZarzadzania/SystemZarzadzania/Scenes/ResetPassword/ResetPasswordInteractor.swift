//
//  ResetPasswordInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol ResetPasswordBusinessLogic {
    func resetPassword(password: String)
}

protocol ResetPasswordDataStore {
    var options: [ResetPasswordDataStoreOptionsKey : Any] { get set }
    var code: String? { get set }
}

enum ResetPasswordDataStoreOptionsKey: String {
    case some
}

class ResetPasswordInteractor: ResetPasswordBusinessLogic, ResetPasswordDataStore {
    
    // MARK: - Properties
    
    var presenter: ResetPasswordPresentationLogic?
    let worker: ResetPasswordWorkerLogic
    var options: [ResetPasswordDataStoreOptionsKey : Any] = [:]
    var code: String?
    let disposeBag = DisposeBag()

    
    // MARK: - Initializer

    init(presenter: ResetPasswordPresentationLogic?, worker: ResetPasswordWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
    func resetPassword(password: String) {
        guard let code = code else { return }
        
        worker.changePassword(password: password, code: code)
            .subscribe(onCompleted: {
                self.presenter?.presentChangePassword(response: .success)
            }, onError: { error in
                self.presenter?.presentChangePassword(response: .error(error))
            }).disposed(by: disposeBag)
    }
}
