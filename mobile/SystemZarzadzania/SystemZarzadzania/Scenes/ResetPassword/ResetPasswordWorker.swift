//
//  ResetPasswordWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol ResetPasswordWorkerLogic {
    func changePassword(password: String, code: String) -> Completable
}

class ResetPasswordWorker: ResetPasswordWorkerLogic {
    
    // MARK: - Properties
    
    let securityApiService: SecurityAPIServiceProtocol
    
    // MARK: - Init
    
    init(securityApiService: SecurityAPIServiceProtocol) {
        self.securityApiService = securityApiService
    }
    

    // MARK: - Methods
    
    func changePassword(password: String, code: String) -> Completable {
        return securityApiService.resetPassword(dto: ResetPasswordRequestDto(password: password, resetCode: code))
    }
}
