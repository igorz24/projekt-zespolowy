//
//  AddProjectRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

@objc protocol AddProjectRoutingLogic {
    //func routeToSomewhere(segue: UIStoryboardSegue?)
}

protocol AddProjectDataPassing {
    var dataStore: AddProjectDataStore? { get }
}

class AddProjectRouter: NSObject, AddProjectRoutingLogic, AddProjectDataPassing {
    
    // MARK: - Properties
    
    weak var viewController: AddProjectViewController?
    var dataStore: AddProjectDataStore?
    
    
    // MARK: - Initializer

    init(viewController: AddProjectViewController?, dataStore: AddProjectDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }

    
    // MARK: - Methods
    
//    func routeToSomewhere(segue: UIStoryboardSegue?) {
//        if let segue = segue {
//            let destinationVC = segue.destination as! SomewhereViewController
//            var destinationDS = destinationVC.router!.dataStore!
//            passDataToSomewhere(source: dataStore!, destination: &destinationDS)
//        } else {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let destinationVC = storyboard.instantiateViewController(withIdentifier: "SomewhereViewController") as! SomewhereViewController
//            var destinationDS = destinationVC.router!.dataStore!
//            passDataToSomewhere(source: dataStore!, destination: &destinationDS)
//            navigateToSomewhere(source: viewController!, destination: destinationVC)
//        }
//    }
    
    
    // MARK: - Navigation
    
//    func navigateToSomewhere(source: AddProjectViewController, destination: SomewhereViewController) {
//        source.show(destination, sender: nil)
//    }
    
    
    // MARK: - Passing data
    
//    func passDataToSomewhere(source: AddProjectDataStore, destination: inout SomewhereDataStore) {
//        destination.name = source.name
//    }
}
