//
//  AddProjectViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

protocol AddProjectDisplayLogic: AnyObject {
    func displaySuccess()
    func showError(error: Error)
}

class AddProjectViewController: UIViewController {
    
    
    
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var modifyButton: UIButton!
    
    
    @IBAction func addAction(_ sender: Any) {
        switch purpose {
        case .add:
            interactor?.addProject(request: AddProject.AddProject.Request(name: nameTextField.text ?? "", description: descriptionTextField.text ?? ""))
        case .modify:
             interactor?.modifyTask(request: AddProject.ModifyProject.Request(name: nameTextField.text ?? "", description: descriptionTextField.text ?? ""))
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Properties
    
    var interactor: AddProjectBusinessLogic?
    var router: (NSObjectProtocol & AddProjectRoutingLogic & AddProjectDataPassing)?
    var purpose: Purpose = .add
    
        
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        AddProjectConfigurator().configure(viewController: self)
    }

    
    // MARK: - Routing
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let scene = segue.identifier {
//            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
//            if let router = router, router.responds(to: selector) {
//                router.perform(selector, with: segue)
//            }
//        }
//    }
    
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    if purpose == .modify {
         modifyButton.setTitle("Modyfikuj", for: .normal)
     } else {
         modifyButton.titleLabel?.text = "Dodaj"
     }
    }
}

extension AddProjectViewController: AddProjectDisplayLogic {
    func displaySuccess() {
        navigationController?.popViewController(animated: true)
    }
    

}
