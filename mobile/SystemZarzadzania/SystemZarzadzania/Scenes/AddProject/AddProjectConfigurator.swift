//
//  AddProjectConfigurator.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class AddProjectConfigurator {
    
    // MARK: - Configuration
    
    func configure(viewController: AddProjectViewController) {
        let presenter = AddProjectPresenter(viewController: viewController)
        let worker = AddProjectWorker(customerAPIService: CustomerFactory.apiService())
        let interactor = AddProjectInteractor(presenter: presenter, worker: worker)
        let router = AddProjectRouter(viewController: viewController, dataStore: interactor)

        viewController.interactor = interactor
        viewController.router = router
    }
}
