//
//  AddProjectWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol AddProjectWorkerLogic {
    func addProject(request: AddProject.AddProject.Request) -> Completable
    func modifyProject(request: AddProject.ModifyProject.Request, projectId: Int) -> Completable
}

class AddProjectWorker: AddProjectWorkerLogic {
    
    let customerAPIService: CustomerAPIServiceProtocol
    
    // MARK: - Init
    
    init(customerAPIService: CustomerAPIServiceProtocol) {
        self.customerAPIService = customerAPIService
    }
    

    func addProject(request: AddProject.AddProject.Request) -> Completable {
        return customerAPIService.addProject(dto: AddProjectRequestDto(name: request.name, description: request.description))
    }
    
    func modifyProject(request: AddProject.ModifyProject.Request, projectId: Int) -> Completable {
        return customerAPIService.modifyProject(dto: ModifyProjectRequestDto(projectId: projectId, name: request.name, description: request.description))
    }
}
