//
//  AddProjectInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol AddProjectBusinessLogic {
    func addProject(request: AddProject.AddProject.Request)
    func modifyTask(request: AddProject.ModifyProject.Request)
}

protocol AddProjectDataStore {
    var projectId: Int? {get set}
    var options: [AddProjectDataStoreOptionsKey : Any] { get set }
}

enum AddProjectDataStoreOptionsKey: String {
    case some
}

class AddProjectInteractor: AddProjectBusinessLogic, AddProjectDataStore {
    
    // MARK: - Properties
    
    var presenter: AddProjectPresentationLogic?
    let worker: AddProjectWorkerLogic
    var options: [AddProjectDataStoreOptionsKey : Any] = [:]
    var projectId: Int?
    let disposeBag = DisposeBag()

    
    // MARK: - Initializer

    init(presenter: AddProjectPresentationLogic?, worker: AddProjectWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
    func addProject(request: AddProject.AddProject.Request) {
        worker.addProject(request: request)
        .subscribe(onCompleted: {
            self.presenter?.presentAddProject(response: .success)
        }, onError: { error in
            self.presenter?.presentAddProject(response: .error(error))
        })
        .disposed(by: disposeBag)
    }
    
    func modifyTask(request: AddProject.ModifyProject.Request) {
        guard let projectId = projectId else {
            presenter?.presentModifyProject(response: .error(AppError(message: "Nie znaleziono id taska")))
            return
        }
        
        worker.modifyProject(request: request, projectId: projectId)
            .subscribe(onCompleted: {
                self.presenter?.presentModifyProject(response: .success)
            }, onError: { error in
                self.presenter?.presentModifyProject(response: .error(error))
            })
            .disposed(by: disposeBag)
    }
}
