//
//  AddProjectPresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol AddProjectPresentationLogic {
    func presentAddProject(response: AddProject.AddProject.Response)
    func presentModifyProject(response: AddProject.ModifyProject.Response)
}

class AddProjectPresenter: AddProjectPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: AddProjectDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: AddProjectDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods
    
    func presentAddProject(response: AddProject.AddProject.Response) {
        switch response {
        case .success:
            viewController?.displaySuccess()
        case .error(let error):
            viewController?.showError(error: error)
        }
    }
    
    func presentModifyProject(response: AddProject.ModifyProject.Response) {
        switch response {
        case .success:
            viewController?.displaySuccess()
        case .error(let error):
            viewController?.showError(error: error)
        }
    }
}
