//
//  AddProjectModels.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum AddProject {
    
    // MARK: - Use cases
    
    enum AddProject {
        struct Request {
            let name: String
            let description: String
        }
        
        enum Response {
            case success
            case error(Error)
        }
    }
    
    enum ModifyProject {
        struct Request {
            let name: String
            let description: String
        }
        
        enum Response {
            case success
            case error(Error)
        }
    }
}
