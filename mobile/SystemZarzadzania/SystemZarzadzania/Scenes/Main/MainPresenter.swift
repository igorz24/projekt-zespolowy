//
//  MainPresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol MainPresentationLogic {
    //  func presentSomething(response: Main.Something.Response)
}

class MainPresenter: MainPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: MainDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: MainDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods
    
//    func presentSomething(response: Main.Something.Response) {
//        let viewModel = Main.Something.ViewModel()
//        viewController?.displaySomething(viewModel: viewModel)
//    }
}
