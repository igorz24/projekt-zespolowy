//
//  MainModels.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum Main {
    
    // MARK: - Use cases
    
    enum Something {
        struct Request {}
        
        enum Response {
            case success
            case error(Error)
        }
        
        struct ViewModel {}
    }
}
