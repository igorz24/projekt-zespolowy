//
//  MainWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol MainWorkerLogic {
    
}

class MainWorker: MainWorkerLogic {
    
    // MARK: - Properties
    
    // MARK: - Init
    
    init() {
        
    }
    

    // MARK: - Methods
}
