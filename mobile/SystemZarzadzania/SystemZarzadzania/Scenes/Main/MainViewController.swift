//
//  MainViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit


protocol MainDisplayLogic: AnyObject {
//    func displaySomething(viewModel: Main.Something.ViewModel)
}

class MainViewController: UITabBarController {
    
    // MARK: - Properties
    
    var interactor: MainBusinessLogic?
    var router: (NSObjectProtocol & MainRoutingLogic & MainDataPassing)?
    
        
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        MainConfigurator().configure(viewController: self)
    }

    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let role = UserDefaultsHelper.getRole()
        
        viewControllers = role.viewControllers
    }
}

extension MainViewController: MainDisplayLogic {

}
