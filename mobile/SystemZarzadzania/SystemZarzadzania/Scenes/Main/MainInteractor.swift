//
//  MainInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol MainBusinessLogic {
//    func doSomething(request: Main.Something.Request)
}

protocol MainDataStore {
    var options: [MainDataStoreOptionsKey : Any] { get set }
}

enum MainDataStoreOptionsKey: String {
    case some
}

class MainInteractor: MainBusinessLogic, MainDataStore {
    
    // MARK: - Properties
    
    var presenter: MainPresentationLogic?
    let worker: MainWorkerLogic
    var options: [MainDataStoreOptionsKey : Any] = [:]

    
    // MARK: - Initializer

    init(presenter: MainPresentationLogic?, worker: MainWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
//    func doSomething(request: Main.Something.Request) {
//        
//    }
}
