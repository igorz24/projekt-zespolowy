//
//  AddCommitWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol AddCommitWorkerLogic {
    func addCommit(request: AddCommit.AddCommit.Request) -> Completable
    func modifyCommit(request: AddCommit.ModifyCommit.Request, commitId: Int) -> Completable
}

class AddCommitWorker: AddCommitWorkerLogic {
    
    let customerAPIService: CustomerAPIServiceProtocol
    
    // MARK: - Init
    
    init(customerAPIService: CustomerAPIServiceProtocol) {
        self.customerAPIService = customerAPIService
    }
    

    // MARK: - Methods
    
    func addCommit(request: AddCommit.AddCommit.Request) -> Completable {
        return customerAPIService.addCommit(dto: AddCommitRequestDto(workTime: request.workTime, taskId: request.taskId, description: request.description))
    }
    
    func modifyCommit(request: AddCommit.ModifyCommit.Request, commitId: Int) -> Completable {
        return customerAPIService.updateCommit(dto: UpdateCommitRequestDto(commitId: commitId, workTime: request.workTime, description: request.description))
    }
}
