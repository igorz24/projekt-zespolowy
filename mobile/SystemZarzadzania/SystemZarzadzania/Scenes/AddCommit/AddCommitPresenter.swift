//
//  AddCommitPresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol AddCommitPresentationLogic {
    func presentAddCommit(response: AddCommit.AddCommit.Response)
    func presentModifyCommit(response: AddCommit.ModifyCommit.Response)
}

class AddCommitPresenter: AddCommitPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: AddCommitDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: AddCommitDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods
    
    func presentAddCommit(response: AddCommit.AddCommit.Response) {
        switch response {
        case .success:
            viewController?.displaySuccess()
        case .error(let error):
            viewController?.showError(error: error)
        }
    }
    
    func presentModifyCommit(response: AddCommit.ModifyCommit.Response) {
        switch response {
        case .success:
            viewController?.displaySuccess()
        case .error(let error):
            viewController?.showError(error: error)
        }

    }
}
