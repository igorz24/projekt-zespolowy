//
//  AddCommitRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

@objc protocol AddCommitRoutingLogic {
    //func routeToSomewhere(segue: UIStoryboardSegue?)
}

protocol AddCommitDataPassing {
    var dataStore: AddCommitDataStore? { get }
}

class AddCommitRouter: NSObject, AddCommitRoutingLogic, AddCommitDataPassing {
    
    // MARK: - Properties
    
    weak var viewController: AddCommitViewController?
    var dataStore: AddCommitDataStore?
    
    
    // MARK: - Initializer

    init(viewController: AddCommitViewController?, dataStore: AddCommitDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }

    
    // MARK: - Methods
    
//    func routeToSomewhere(segue: UIStoryboardSegue?) {
//        if let segue = segue {
//            let destinationVC = segue.destination as! SomewhereViewController
//            var destinationDS = destinationVC.router!.dataStore!
//            passDataToSomewhere(source: dataStore!, destination: &destinationDS)
//        } else {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let destinationVC = storyboard.instantiateViewController(withIdentifier: "SomewhereViewController") as! SomewhereViewController
//            var destinationDS = destinationVC.router!.dataStore!
//            passDataToSomewhere(source: dataStore!, destination: &destinationDS)
//            navigateToSomewhere(source: viewController!, destination: destinationVC)
//        }
//    }
    
    
    // MARK: - Navigation
    
//    func navigateToSomewhere(source: AddCommitViewController, destination: SomewhereViewController) {
//        source.show(destination, sender: nil)
//    }
    
    
    // MARK: - Passing data
    
//    func passDataToSomewhere(source: AddCommitDataStore, destination: inout SomewhereDataStore) {
//        destination.name = source.name
//    }
}
