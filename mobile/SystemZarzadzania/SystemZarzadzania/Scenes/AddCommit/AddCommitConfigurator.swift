//
//  AddCommitConfigurator.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class AddCommitConfigurator {
    
    // MARK: - Configuration
    
    func configure(viewController: AddCommitViewController) {
        let presenter = AddCommitPresenter(viewController: viewController)
        let worker = AddCommitWorker(customerAPIService: CustomerFactory.apiService())
        let interactor = AddCommitInteractor(presenter: presenter, worker: worker)
        let router = AddCommitRouter(viewController: viewController, dataStore: interactor)

        viewController.interactor = interactor
        viewController.router = router
    }
}
