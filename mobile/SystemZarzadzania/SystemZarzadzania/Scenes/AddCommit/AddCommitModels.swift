//
//  AddCommitModels.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum AddCommit {
    
    // MARK: - Use cases
    
    enum AddCommit {
        struct Request {
            let taskId: Int
            let workTime: Int
            let description: String
        }
        
        enum Response {
            case success
            case error(Error)
        }
    }
    
    enum ModifyCommit {
        struct Request {
            let workTime: Int
            let description: String
        }
        
        enum Response {
            case success
            case error(Error)
        }
    }
}
