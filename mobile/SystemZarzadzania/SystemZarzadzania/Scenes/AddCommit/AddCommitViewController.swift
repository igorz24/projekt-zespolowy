//
//  AddCommitViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

enum Purpose {
    case add
    case modify
}

protocol AddCommitDisplayLogic: AnyObject {
    func displaySuccess()
    func showError(error: Error)
}

class AddCommitViewController: UIViewController {
    
    @IBOutlet weak var taskIdTextFIeld: UITextField!
    @IBOutlet weak var workTimeTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var modifyButton: UIButton!
    
    @IBAction func modifyAction(_ sender: Any) {
        switch purpose {
        case .add:
            interactor?.addCommit(request: AddCommit.AddCommit.Request(taskId: Int(taskIdTextFIeld.text ?? "-1") ?? -1, workTime: Int(workTimeTextField.text ?? "-1") ?? -1, description: descriptionTextField.text ?? ""))
        case .modify:
            interactor?.modifyCommit(request: AddCommit.ModifyCommit.Request(workTime: Int(workTimeTextField.text ?? "-1") ?? -1, description: descriptionTextField.text ?? ""))
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Properties
    
    var interactor: AddCommitBusinessLogic?
    var router: (NSObjectProtocol & AddCommitRoutingLogic & AddCommitDataPassing)?
    var purpose: Purpose = .add
    
        
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        AddCommitConfigurator().configure(viewController: self)
    }
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if purpose == .modify {
            taskIdTextFIeld.isHidden = true
            modifyButton.setTitle("Modyfikuj", for: .normal)
        } else {
            modifyButton.titleLabel?.text = "Dodaj"
        }
    }
}

extension AddCommitViewController: AddCommitDisplayLogic {
    func displaySuccess() {
        navigationController?.popViewController(animated: true)
    }
}
