//
//  AddCommitInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol AddCommitBusinessLogic {
    func addCommit(request: AddCommit.AddCommit.Request)
    func modifyCommit(request: AddCommit.ModifyCommit.Request)
}

protocol AddCommitDataStore {
    var commitId: Int? {get set}
    var options: [AddCommitDataStoreOptionsKey : Any] { get set }
}

enum AddCommitDataStoreOptionsKey: String {
    case some
}

class AddCommitInteractor: AddCommitBusinessLogic, AddCommitDataStore {
    
    // MARK: - Properties
    
    var presenter: AddCommitPresentationLogic?
    let worker: AddCommitWorkerLogic
    var options: [AddCommitDataStoreOptionsKey : Any] = [:]
    var commitId: Int?
    var disposeBag = DisposeBag()

    
    // MARK: - Initializer

    init(presenter: AddCommitPresentationLogic?, worker: AddCommitWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
    func addCommit(request: AddCommit.AddCommit.Request) {
        worker.addCommit(request: request)
        .subscribe(onCompleted: {
            self.presenter?.presentAddCommit(response: .success)
        }, onError: { error in
            self.presenter?.presentAddCommit(response: .error(error))
        })
        .disposed(by: disposeBag)
    }
    
    func modifyCommit(request: AddCommit.ModifyCommit.Request) {
        guard let commitId = commitId else {
            presenter?.presentModifyCommit(response: .error(AppError(message: "Nie znaleziono id commita")))
            return
        }
        
        worker.modifyCommit(request: request, commitId: commitId)
            .subscribe(onCompleted: {
                self.presenter?.presentModifyCommit(response: .success)
            }, onError: { error in
                self.presenter?.presentModifyCommit(response: .error(error))
            })
            .disposed(by: disposeBag)
        
    }
}
