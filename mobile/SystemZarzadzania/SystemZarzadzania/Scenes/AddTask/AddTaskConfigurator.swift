//
//  AddTaskConfigurator.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class AddTaskConfigurator {
    
    // MARK: - Configuration
    
    func configure(viewController: AddTaskViewController) {
        let presenter = AddTaskPresenter(viewController: viewController)
        let worker = AddTaskWorker(customerAPIService: CustomerFactory.apiService())
        let interactor = AddTaskInteractor(presenter: presenter, worker: worker)
        let router = AddTaskRouter(viewController: viewController, dataStore: interactor)

        viewController.interactor = interactor
        viewController.router = router
    }
}
