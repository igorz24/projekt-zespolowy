//
//  AddTaskViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

protocol AddTaskDisplayLogic: AnyObject {
    func displaySuccess()
    func showError(error: Error)
}

class AddTaskViewController: UIViewController {
    
    
    @IBOutlet weak var taskIdLabel: UITextField!
    @IBOutlet weak var estimatedTimeLabel: UITextField!
    
    @IBOutlet weak var modifyButton: UIButton!
    @IBOutlet weak var descriptionLabel: UITextField!
    
    @IBAction func ModifyAction(_ sender: Any) {
        switch purpose {
        case .add:
            interactor?.addTask(request: AddTask.AddTask.Request(projectId: Int(taskIdLabel.text ?? "-1") ?? -1, estimationTime: Int(estimatedTimeLabel.text ?? "-1") ?? -1, description: descriptionLabel.text ?? ""))
        case .modify:
            interactor?.modifyTask(request: AddTask.ModifyTask.Request(estimationTime: Int(estimatedTimeLabel.text ?? "-1") ?? -1, description: descriptionLabel.text ?? ""))
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Properties
    
    var interactor: AddTaskBusinessLogic?
    var router: (NSObjectProtocol & AddTaskRoutingLogic & AddTaskDataPassing)?
    var purpose: Purpose = .add
    
        
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        AddTaskConfigurator().configure(viewController: self)
    }

    
    // MARK: - Routing
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let scene = segue.identifier {
//            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
//            if let router = router, router.responds(to: selector) {
//                router.perform(selector, with: segue)
//            }
//        }
//    }
    
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if purpose == .modify {
            taskIdLabel.isHidden = true
            modifyButton.setTitle("Modyfikuj", for: .normal)
        } else {
            modifyButton.titleLabel?.text = "Dodaj"
        }

    }
}

extension AddTaskViewController: AddTaskDisplayLogic {
    func displaySuccess() {
        navigationController?.popViewController(animated: true)
    }
}
