//
//  AddTaskRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

@objc protocol AddTaskRoutingLogic {
    //func routeToSomewhere(segue: UIStoryboardSegue?)
}

protocol AddTaskDataPassing {
    var dataStore: AddTaskDataStore? { get }
}

class AddTaskRouter: NSObject, AddTaskRoutingLogic, AddTaskDataPassing {
    
    // MARK: - Properties
    
    weak var viewController: AddTaskViewController?
    var dataStore: AddTaskDataStore?
    
    
    // MARK: - Initializer

    init(viewController: AddTaskViewController?, dataStore: AddTaskDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }

    
    // MARK: - Methods
    
//    func routeToSomewhere(segue: UIStoryboardSegue?) {
//        if let segue = segue {
//            let destinationVC = segue.destination as! SomewhereViewController
//            var destinationDS = destinationVC.router!.dataStore!
//            passDataToSomewhere(source: dataStore!, destination: &destinationDS)
//        } else {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let destinationVC = storyboard.instantiateViewController(withIdentifier: "SomewhereViewController") as! SomewhereViewController
//            var destinationDS = destinationVC.router!.dataStore!
//            passDataToSomewhere(source: dataStore!, destination: &destinationDS)
//            navigateToSomewhere(source: viewController!, destination: destinationVC)
//        }
//    }
    
    
    // MARK: - Navigation
    
//    func navigateToSomewhere(source: AddTaskViewController, destination: SomewhereViewController) {
//        source.show(destination, sender: nil)
//    }
    
    
    // MARK: - Passing data
    
//    func passDataToSomewhere(source: AddTaskDataStore, destination: inout SomewhereDataStore) {
//        destination.name = source.name
//    }
}
