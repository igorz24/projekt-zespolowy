//
//  AddTaskWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol AddTaskWorkerLogic {
    func addTask(request: AddTask.AddTask.Request) -> Completable
    func modifyTask(request: AddTask.ModifyTask.Request, taskId: Int) -> Completable
}

class AddTaskWorker: AddTaskWorkerLogic {
    
    let customerAPIService: CustomerAPIServiceProtocol
    
    // MARK: - Init
    
    init(customerAPIService: CustomerAPIServiceProtocol) {
        self.customerAPIService = customerAPIService
    }
    

    func addTask(request: AddTask.AddTask.Request) -> Completable {
        return customerAPIService.addTask(dto: AddTaskRequestDto(projectId: request.projectId, estimationTime: request.estimationTime, description: request.description))
    }
    
    func modifyTask(request: AddTask.ModifyTask.Request, taskId: Int) -> Completable {
        return customerAPIService.updateTask(dto: ModifyTaskRequestDto(taskId: taskId, estimationTime: request.estimationTime, description: request.description))
    }
}
