//
//  AddTaskPresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol AddTaskPresentationLogic {
    func presentAddTask(response: AddTask.AddTask.Response)
    func presentModifyTask(response: AddTask.ModifyTask.Response)
}

class AddTaskPresenter: AddTaskPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: AddTaskDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: AddTaskDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods
    
func presentAddTask(response: AddTask.AddTask.Response) {
    switch response {
    case .success:
        viewController?.displaySuccess()
    case .error(let error):
        viewController?.showError(error: error)
    }
}

func presentModifyTask(response: AddTask.ModifyTask.Response) {
    switch response {
    case .success:
        viewController?.displaySuccess()
    case .error(let error):
        viewController?.showError(error: error)
    }

}
}
