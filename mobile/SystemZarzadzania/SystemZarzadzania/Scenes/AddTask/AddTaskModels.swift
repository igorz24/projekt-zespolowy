//
//  AddTaskModels.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum AddTask {
    
    // MARK: - Use cases
    
    enum AddTask {
        struct Request {
            let projectId: Int
            let estimationTime: Int
            let description: String
        }
        
        enum Response {
            case success
            case error(Error)
        }
    }
    
    enum ModifyTask {
        struct Request {
            let estimationTime: Int
            let description: String
        }
        
        enum Response {
            case success
            case error(Error)
        }
    }
}
