//
//  AddTaskInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol AddTaskBusinessLogic {
    func addTask(request: AddTask.AddTask.Request)
    func modifyTask(request: AddTask.ModifyTask.Request)
}

protocol AddTaskDataStore {
    var taskId: Int? {get set}
    var options: [AddTaskDataStoreOptionsKey : Any] { get set }
}

enum AddTaskDataStoreOptionsKey: String {
    case some
}

class AddTaskInteractor: AddTaskBusinessLogic, AddTaskDataStore {
    
    // MARK: - Properties
    
    var presenter: AddTaskPresentationLogic?
    let worker: AddTaskWorkerLogic
    var options: [AddTaskDataStoreOptionsKey : Any] = [:]
    var taskId: Int?
    var disposeBag = DisposeBag()

    
    // MARK: - Initializer

    init(presenter: AddTaskPresentationLogic?, worker: AddTaskWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
func addTask(request: AddTask.AddTask.Request) {
    worker.addTask(request: request)
    .subscribe(onCompleted: {
        self.presenter?.presentAddTask(response: .success)
    }, onError: { error in
        self.presenter?.presentAddTask(response: .error(error))
    })
    .disposed(by: disposeBag)
}

func modifyTask(request: AddTask.ModifyTask.Request) {
    guard let taskId = taskId else {
        presenter?.presentModifyTask(response: .error(AppError(message: "Nie znaleziono id taska")))
        return
    }
    
    worker.modifyTask(request: request, taskId: taskId)
        .subscribe(onCompleted: {
            self.presenter?.presentModifyTask(response: .success)
        }, onError: { error in
            self.presenter?.presentModifyTask(response: .error(error))
        })
        .disposed(by: disposeBag)
    
}
}
