//
//  AddUserToProjectViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import UIKit

protocol AddUserDelegate: AnyObject {
    func addUser(projectId: Int, userId: Int)
}

class AddUserToProjectViewController: UIViewController {
    
    
    @IBOutlet weak var projectIdField: UITextField!
    @IBAction func okAction(_ sender: Any) {
        delegate?.addUser(projectId: Int(projectIdField.text ?? "") ?? 0, userId: userId)
        dismiss(animated: true)
    }

    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    var userId = 0
    weak var delegate: AddUserDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
