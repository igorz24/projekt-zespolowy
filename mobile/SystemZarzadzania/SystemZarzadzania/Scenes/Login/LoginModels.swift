//
//  LoginModels.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum Login {
    
    // MARK: - Use cases
    
    enum Login {
        struct Request {
            let email: String
            let password: String
        }
        
        enum Response {
            case success
            case error(Error)
        }
    }
    
    enum ResetPassword {
        struct Request {
            let email: String
        }
        
        enum Response {
            case success
            case error(Error)
        }
    }
}
