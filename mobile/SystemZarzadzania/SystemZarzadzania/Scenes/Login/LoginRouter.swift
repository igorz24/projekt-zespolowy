//
//  LoginRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

@objc protocol LoginRoutingLogic {
    func routeToMain()
    func routeToRegister()
}

protocol LoginDataPassing {
    var dataStore: LoginDataStore? { get }
}

class LoginRouter: NSObject, LoginRoutingLogic, LoginDataPassing {
    
    // MARK: - Properties
    
    weak var viewController: LoginViewController?
    var dataStore: LoginDataStore?
    
    
    // MARK: - Initializer

    init(viewController: LoginViewController?, dataStore: LoginDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }

    
    // MARK: - Methods
    
    func routeToMain() {
        let vc = MainViewController()
        
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func routeToRegister() {
        viewController?.navigationController?.pushViewController(UIStoryboard.viewController(for: .reigster), animated: true)
    }
}
