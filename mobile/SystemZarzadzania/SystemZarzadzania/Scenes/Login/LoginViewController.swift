//
//  LoginViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol LoginDisplayLogic: AnyObject {
    func displayLoginSuccess()
    func displayResetPasswordSuccess()
    func showError(error: Error)
}

class LoginViewController: UIViewController {
    
    
    // MARK: - IBOutlets
    
    
    @IBOutlet weak var forgotPasswordStackView: UIStackView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgetPasswordEmailTextFIeld: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var sendResetPasswordButton: UIButton!
    
    // MARK: - IBActions
    
    
    @IBAction func LoginAction(_ sender: Any) {
        interactor?.login(request: Login.Login.Request(email: emailTextField.text!, password: passwordTextField.text!))
    }
    
        
    @IBAction func RegisterAction(_ sender: Any) {
        router?.routeToRegister()
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        forgotPasswordStackView.isHidden.toggle()
    }
    
    @IBAction func sendResetPasswordAction(_ sender: Any) {
        interactor?.resetPassword(request: Login.ResetPassword.Request(email: forgetPasswordEmailTextFIeld.text ?? ""))
    }
    
    // MARK: - Properties
    
    var interactor: LoginBusinessLogic?
    var router: (NSObjectProtocol & LoginRoutingLogic & LoginDataPassing)?
    var disposeBag = DisposeBag()
    
        
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        LoginConfigurator().configure(viewController: self)
    }

        
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        forgotPasswordStackView.isHidden = true
        setupRx()
    }
    
    func setupRx() {
        let emailValid = emailTextField.rx.text.orEmpty.map { $0.count > 0 }
        let passwordValid = passwordTextField.rx.text.orEmpty.map { $0.count > 0 }
        
        Observable.combineLatest(emailValid, passwordValid) { $0 && $1 }
            .bind(to: loginButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        let forgetEmailValid = forgetPasswordEmailTextFIeld.rx.text.orEmpty.map { $0.count > 0 }
        
        forgetEmailValid.bind(to: sendResetPasswordButton.rx.isEnabled)
        .disposed(by: disposeBag)
    }

}

extension LoginViewController: LoginDisplayLogic {
    func displayResetPasswordSuccess() {
        showAlert(withTitle: "Reset Hasła", message: "Sprawdź email po dalsze instrukcje")
    }
    
    func displayLoginSuccess() {
        router?.routeToMain()
    }
}
