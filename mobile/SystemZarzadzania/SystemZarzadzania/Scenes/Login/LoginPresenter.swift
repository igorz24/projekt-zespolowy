//
//  LoginPresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol LoginPresentationLogic {
    func presentLogin(response: Login.Login.Response)
    func presentResetPassword(response: Login.ResetPassword.Response)
}

class LoginPresenter: LoginPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: LoginDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: LoginDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods

    func presentLogin(response: Login.Login.Response) {
        switch response {
        case .success:
            viewController?.displayLoginSuccess()
        case .error(let error):
            viewController?.showError(error: error)
        }
    }
    
    func presentResetPassword(response: Login.ResetPassword.Response) {
        switch response {
        case .success:
            viewController?.displayResetPasswordSuccess()
        case .error(let error):
            viewController?.showError(error: error)
        }
    }
}
