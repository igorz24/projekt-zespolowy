//
//  LoginWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol LoginWorkerLogic {
    func login(request: Login.Login.Request) -> Single<LoginResponseDto>
    func resetPassword(request: Login.ResetPassword.Request) -> Completable
}

class LoginWorker: LoginWorkerLogic {
    
    // MARK: - Properties
    
    let securityAPIService: SecurityAPIServiceProtocol
    let securityRepository: SecurityRepositoryProtocol
    
    
    // MARK: - Init
    
    init(securityAPIService: SecurityAPIServiceProtocol, securityRepository: SecurityRepositoryProtocol) {
        self.securityAPIService = securityAPIService
        self.securityRepository = securityRepository
    }
    

    // MARK: - Methods
    
    func login(request: Login.Login.Request) -> Single<LoginResponseDto> {
        return securityAPIService.login(dto: LoginRequestDto(email: request.email, password: request.password))
            .do(onSuccess: { [weak self] response in
                try? self?.securityRepository.save(authEntity: AuthEntity(token: response.token))
                UserDefaultsHelper.save(string: response.profile.role.rawValue, for: .role)
            })
    }
    
    func resetPassword(request: Login.ResetPassword.Request) -> Completable {
        return securityAPIService.sendResetCode(dto: ResetCodeRequestDto(email: request.email))
    }
}
