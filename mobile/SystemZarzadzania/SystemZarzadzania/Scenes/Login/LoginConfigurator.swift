//
//  LoginConfigurator.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class LoginConfigurator {
    
    // MARK: - Configuration
    
    func configure(viewController: LoginViewController) {
        let presenter = LoginPresenter(viewController: viewController)
        let worker = LoginWorker(securityAPIService: SecurityFactory.apiService(), securityRepository: SecurityFactory.repository())
        let interactor = LoginInteractor(presenter: presenter, worker: worker)
        let router = LoginRouter(viewController: viewController, dataStore: interactor)

        viewController.interactor = interactor
        viewController.router = router
    }
}
