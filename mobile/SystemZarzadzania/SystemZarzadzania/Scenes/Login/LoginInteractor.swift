//
//  LoginInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 04/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol LoginBusinessLogic {
    func login(request: Login.Login.Request)
    func resetPassword(request: Login.ResetPassword.Request)
}

protocol LoginDataStore {
    var options: [LoginDataStoreOptionsKey : Any] { get set }
}

enum LoginDataStoreOptionsKey: String {
    case some
}

class LoginInteractor: LoginBusinessLogic, LoginDataStore {
    
    // MARK: - Properties
    
    var presenter: LoginPresentationLogic?
    let worker: LoginWorkerLogic
    var options: [LoginDataStoreOptionsKey : Any] = [:]
    let disposeBag = DisposeBag()

    
    // MARK: - Initializer

    init(presenter: LoginPresentationLogic?, worker: LoginWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
    func login(request: Login.Login.Request) {
        worker.login(request: request)
            .subscribe(onSuccess: { [weak self] _ in
                self?.presenter?.presentLogin(response: .success)
                }, onError: { [weak self] error in
                    self?.presenter?.presentLogin(response: .error(error))
            })
        .disposed(by: disposeBag)
    }
    
    func resetPassword(request: Login.ResetPassword.Request) {
        worker.resetPassword(request: request)
            .subscribe(onCompleted: { [weak self] in
                self?.presenter?.presentResetPassword(response: .success)
                }, onError: { [weak self] error in
                    self?.presenter?.presentResetPassword(response: .error(error))
            })
            .disposed(by: disposeBag)
    }
}
