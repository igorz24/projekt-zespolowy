//
//  ProjectDetailsViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

protocol ProjectDetailsDisplayLogic: AnyObject {
    func showError(error: Error)
    func displayProjectDetails(viewModel: [Project.GetProjects.ViewModel.Developer])
    func displayReport(viewModel: ReportModel)
}

class ProjectDetailsViewController: UIViewController {
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func generateReportAction(_ sender: Any) {
        interactor?.generateReport()
    }
    
    // MARK: - Properties
    
    @IBOutlet weak var tableView: UITableView!
    var interactor: ProjectDetailsBusinessLogic?
    var router: (NSObjectProtocol & ProjectDetailsRoutingLogic & ProjectDetailsDataPassing)?
    var data: [Project.GetProjects.ViewModel.Developer] = []
    
        
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ProjectDetailsConfigurator().configure(viewController: self)
    }

    
    // MARK: - Routing
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let scene = segue.identifier {
//            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
//            if let router = router, router.responds(to: selector) {
//                router.perform(selector, with: segue)
//            }
//        }
//    }
    
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor?.getProjectDetails(request: ProjectDetails.GetProjectDetails.Request())
    }
}

extension ProjectDetailsViewController: ProjectDetailsDisplayLogic {
    
    func displayReport(viewModel: ReportModel) {
        guard let vc = UIStoryboard.viewController(for: .projectReport) as? ProjectReportViewController else { return }
        vc.model = viewModel
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func displayProjectDetails(viewModel: [Project.GetProjects.ViewModel.Developer]) {
        data = viewModel
        tableView.reloadData()
    }

}

extension ProjectDetailsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProjectDetailsTableViewCell.cellIdentifier) as? ProjectDetailsTableViewCell else {
            fatalError()
        }
        
        cell.setup(data: data[indexPath.row])
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
}
