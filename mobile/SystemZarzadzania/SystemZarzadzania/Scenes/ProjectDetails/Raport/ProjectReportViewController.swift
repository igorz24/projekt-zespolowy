//
//  ProjectReportViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import UIKit

struct ReportModel {
    let developerNumber: Int
    let commitNumber: Int
    let taskNumber: Int
    let estimatedTime: Int
    let timeSpend: Int
    let commitsWaitingForApproval: Int
}

class ProjectReportViewController: UIViewController {

    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var numberOfDevelopersLabel: UILabel!
    @IBOutlet weak var commitsWaitingForApproval: UILabel!
    @IBOutlet weak var timeAlreadySpentLabel: UILabel!
    @IBOutlet weak var estimatedTimeLabel: UILabel!
    @IBOutlet weak var numberOfTasksLabel: UILabel!
    @IBOutlet weak var numberOfCommitsLabel: UILabel!
    
    
    var model: ReportModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        numberOfDevelopersLabel.text = String(model?.developerNumber ?? 0)
        commitsWaitingForApproval.text = String(model?.commitsWaitingForApproval ?? 0)
        timeAlreadySpentLabel.text = String(model?.timeSpend ?? 0)
        estimatedTimeLabel.text = String(model?.estimatedTime ?? 0)
        numberOfTasksLabel.text = String(model?.taskNumber ?? 0)
        numberOfCommitsLabel.text = String(model?.commitNumber ?? 0)
    }
}
