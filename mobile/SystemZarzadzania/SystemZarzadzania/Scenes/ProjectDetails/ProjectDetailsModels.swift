//
//  ProjectDetailsModels.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum ProjectDetails {
    
    // MARK: - Use cases
    
    enum GetProjectDetails {
        struct Request {
        }
        
        enum Response {
            case success(Project.GetProjects.ViewModel)
            case error(Error)
        }
        
        struct ViewModel {
            let id: Int
            let firstName: Int
            let lastName: String
        }
    }
    
    enum GenerateReport {
        struct Request {
        }
        
        enum Response {
            case success(GenerateReportResponseDto)
            case error(Error)
        }
    }
}
