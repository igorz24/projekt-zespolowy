//
//  ProjectDetailsPresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol ProjectDetailsPresentationLogic {
    func presentProjectDetails(response: ProjectDetails.GetProjectDetails.Response)
    func presentGenerateReport(response: ProjectDetails.GenerateReport.Response)
}

class ProjectDetailsPresenter: ProjectDetailsPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: ProjectDetailsDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: ProjectDetailsDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods
    
    func presentProjectDetails(response: ProjectDetails.GetProjectDetails.Response) {
        switch response {
        case .success(let project):
            viewController?.displayProjectDetails(viewModel: project.developers)
        case .error(let error):
            viewController?.showError(error: error)
        }
    }
    
    func presentGenerateReport(response: ProjectDetails.GenerateReport.Response) {
        switch response {
        case .success(let report):
            let viewModel = ReportModel(developerNumber: report.numberOfDevelopers, commitNumber: report.numberOfCommits, taskNumber: report.numberOfTasks, estimatedTime: report.totalEstimatedTime, timeSpend: report.totalTimeSpent, commitsWaitingForApproval: report.commitsWaitingForApproval)
            viewController?.displayReport(viewModel: viewModel)
        case .error(let error):
            viewController?.showError(error: error)
        }
    }
}
