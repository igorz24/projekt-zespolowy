//
//  ProjectDetailsWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol ProjectDetailsWorkerLogic {
    func generateReport(projectId: Int) -> Single<GenerateReportResponseDto>
}

class ProjectDetailsWorker: ProjectDetailsWorkerLogic {
    
    // MARK: - Properties
    
    let customerAPIService: CustomerAPIServiceProtocol
    
    // MARK: - Init
    
    init(customerAPIService: CustomerAPIServiceProtocol) {
        self.customerAPIService = customerAPIService
    }
    

    // MARK: - Methods
    
    func generateReport(projectId: Int) -> Single<GenerateReportResponseDto> {
        return customerAPIService.generateReport(dto: GenerateReportRequestDto(projectId: projectId))
    }
}
