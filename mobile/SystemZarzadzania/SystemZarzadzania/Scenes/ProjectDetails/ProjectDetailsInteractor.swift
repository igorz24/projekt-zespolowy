//
//  ProjectDetailsInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol ProjectDetailsBusinessLogic {
    func getProjectDetails(request: ProjectDetails.GetProjectDetails.Request)
    func generateReport()
}

protocol ProjectDetailsDataStore {
    var viewModel: Project.GetProjects.ViewModel? { get set }
    var options: [ProjectDetailsDataStoreOptionsKey : Any] { get set }
}

enum ProjectDetailsDataStoreOptionsKey: String {
    case some
}

class ProjectDetailsInteractor: ProjectDetailsBusinessLogic, ProjectDetailsDataStore {
    
    // MARK: - Properties
    
    var presenter: ProjectDetailsPresentationLogic?
    let worker: ProjectDetailsWorkerLogic
    var options: [ProjectDetailsDataStoreOptionsKey : Any] = [:]
    var viewModel: Project.GetProjects.ViewModel?
    let disposeBag = DisposeBag()

    
    // MARK: - Initializer

    init(presenter: ProjectDetailsPresentationLogic?, worker: ProjectDetailsWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
    func getProjectDetails(request: ProjectDetails.GetProjectDetails.Request) {
        guard let viewModel = viewModel else { return }
        
        presenter?.presentProjectDetails(response: .success(viewModel))
    }
    
    func generateReport() {
        guard let viewModel = viewModel else { return }
        worker.generateReport(projectId: viewModel.projectId)
            .subscribe(onSuccess: { response in
                self.presenter?.presentGenerateReport(response: .success(response))
            }, onError: { error in
                self.presenter?.presentGenerateReport(response: .error(error))
        })
        .disposed(by: disposeBag)
    }
}
