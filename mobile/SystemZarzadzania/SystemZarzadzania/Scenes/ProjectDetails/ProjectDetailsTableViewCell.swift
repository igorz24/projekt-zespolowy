//
//  ProjectDetailsTableViewCell.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import UIKit

class ProjectDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var developerId: UILabel!
    
    @IBOutlet weak var developerName: UILabel!
    @IBOutlet weak var developerLastName: UILabel!
    
    static let cellIdentifier = "ProjectDetailsTableViewCellIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(data: Project.GetProjects.ViewModel.Developer) {
        developerId.text = String(data.id)
        developerName.text = data.firstName
        developerLastName.text = data.lastName
    }

}
