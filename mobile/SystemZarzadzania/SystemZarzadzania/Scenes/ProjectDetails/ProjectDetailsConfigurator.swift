//
//  ProjectDetailsConfigurator.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 09/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class ProjectDetailsConfigurator {
    
    // MARK: - Configuration
    
    func configure(viewController: ProjectDetailsViewController) {
        let presenter = ProjectDetailsPresenter(viewController: viewController)
        let worker = ProjectDetailsWorker(customerAPIService: CustomerFactory.apiService())
        let interactor = ProjectDetailsInteractor(presenter: presenter, worker: worker)
        let router = ProjectDetailsRouter(viewController: viewController, dataStore: interactor)

        viewController.interactor = interactor
        viewController.router = router
    }
}
