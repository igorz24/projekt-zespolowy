//
//  MoreTableViewCell.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import UIKit

class MoreTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    static let cellIdentifier = "MoreTableViewCellIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
