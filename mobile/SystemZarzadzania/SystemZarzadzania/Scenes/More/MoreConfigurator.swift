//
//  MoreConfigurator.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class MoreConfigurator {
    
    // MARK: - Configuration
    
    func configure(viewController: MoreViewController) {
        let presenter = MorePresenter(viewController: viewController)
        let worker = MoreWorker(securityRepository: SecurityFactory.repository())
        let interactor = MoreInteractor(presenter: presenter, worker: worker)
        let router = MoreRouter(viewController: viewController, dataStore: interactor)

        viewController.interactor = interactor
        viewController.router = router
    }
}
