//
//  MorePresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol MorePresentationLogic {
    func presentLogout()
}

class MorePresenter: MorePresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: MoreDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: MoreDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods
    
    func presentLogout() {
        viewController?.displayLogout()
    }
}
