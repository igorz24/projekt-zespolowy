//
//  MoreWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol MoreWorkerLogic {
    func logout() -> Completable
}

class MoreWorker: MoreWorkerLogic {
    
    // MARK: - Properties
    
    let securityRepository: SecurityRepositoryProtocol
    
    // MARK: - Init
    
    init(securityRepository: SecurityRepositoryProtocol) {
        self.securityRepository = securityRepository
    }
    

    // MARK: - Methods
    
    func logout() -> Completable {
        
        let completable = Completable.create(subscribe: { [weak self] observer -> Disposable in
            
            self?.securityRepository.clearAuthorization()
            observer(.completed)
            return Disposables.create()
        })

        return completable
    }
}
