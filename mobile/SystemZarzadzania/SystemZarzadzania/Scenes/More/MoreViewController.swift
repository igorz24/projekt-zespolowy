//
//  MoreViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

protocol MoreDisplayLogic: AnyObject {
    func displayLogout()
}

class MoreViewController: UIViewController {
    
    // MARK: - Properties
    
    var interactor: MoreBusinessLogic?
    var router: (NSObjectProtocol & MoreRoutingLogic & MoreDataPassing)?
    var data: [More.Option] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        MoreConfigurator().configure(viewController: self)
    }

    
    // MARK: - Routing
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let scene = segue.identifier {
//            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
//            if let router = router, router.responds(to: selector) {
//                router.perform(selector, with: segue)
//            }
//        }
//    }
    
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let role = UserDefaultsHelper.getRole()
        data = role.additionalOptions
        tableView.reloadData()
    }
}

extension MoreViewController: MoreDisplayLogic {
    func displayLogout() {
        UIApplication.appDelegate()?.rootRouter?.routeToLogin()
    }
}

extension MoreViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MoreTableViewCell.cellIdentifier) as? MoreTableViewCell else { fatalError() }
        
        cell.nameLabel.text = data[indexPath.row].name
        
        return cell
    }
    
    
}

extension MoreViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let option = data[indexPath.row]
        
        switch option {
        case .logout:
            interactor?.logout()
        }
    }
}
