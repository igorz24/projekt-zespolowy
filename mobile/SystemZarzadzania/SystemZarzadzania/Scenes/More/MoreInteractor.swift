//
//  MoreInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol MoreBusinessLogic {
    func logout()
}

protocol MoreDataStore {
    var options: [MoreDataStoreOptionsKey : Any] { get set }
}

enum MoreDataStoreOptionsKey: String {
    case some
}

class MoreInteractor: MoreBusinessLogic, MoreDataStore {
    
    // MARK: - Properties
    
    var presenter: MorePresentationLogic?
    let worker: MoreWorkerLogic
    var options: [MoreDataStoreOptionsKey : Any] = [:]
    let disposeBag = DisposeBag()

    
    // MARK: - Initializer

    init(presenter: MorePresentationLogic?, worker: MoreWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
    func logout() {
        worker.logout().subscribe(onCompleted: {
            self.presenter?.presentLogout()
            }).disposed(by: disposeBag)
    }
}
