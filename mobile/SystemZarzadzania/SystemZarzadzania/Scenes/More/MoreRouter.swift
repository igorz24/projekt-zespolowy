//
//  MoreRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 30/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

@objc protocol MoreRoutingLogic {
    //func routeToSomewhere(segue: UIStoryboardSegue?)
}

protocol MoreDataPassing {
    var dataStore: MoreDataStore? { get }
}

class MoreRouter: NSObject, MoreRoutingLogic, MoreDataPassing {
    
    // MARK: - Properties
    
    weak var viewController: MoreViewController?
    var dataStore: MoreDataStore?
    
    
    // MARK: - Initializer

    init(viewController: MoreViewController?, dataStore: MoreDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }

    
    // MARK: - Methods
    
//    func routeToSomewhere(segue: UIStoryboardSegue?) {
//        if let segue = segue {
//            let destinationVC = segue.destination as! SomewhereViewController
//            var destinationDS = destinationVC.router!.dataStore!
//            passDataToSomewhere(source: dataStore!, destination: &destinationDS)
//        } else {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let destinationVC = storyboard.instantiateViewController(withIdentifier: "SomewhereViewController") as! SomewhereViewController
//            var destinationDS = destinationVC.router!.dataStore!
//            passDataToSomewhere(source: dataStore!, destination: &destinationDS)
//            navigateToSomewhere(source: viewController!, destination: destinationVC)
//        }
//    }
    
    
    // MARK: - Navigation
    
//    func navigateToSomewhere(source: MoreViewController, destination: SomewhereViewController) {
//        source.show(destination, sender: nil)
//    }
    
    
    // MARK: - Passing data
    
//    func passDataToSomewhere(source: MoreDataStore, destination: inout SomewhereDataStore) {
//        destination.name = source.name
//    }
}
