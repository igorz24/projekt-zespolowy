//
//  UserTableViewCell.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    
    @IBOutlet weak var firstNameLabel: UILabel!
    
    @IBOutlet weak var lastNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static let cellIdentifier = "UserTableViewCell"
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(viewModel: User.GetUsers.ViewModel) {
        idLabel.text = String(viewModel.id)
        emailLabel.text = viewModel.email
        roleLabel.text = viewModel.role
        firstNameLabel.text = viewModel.firstName
        lastNameLabel.text = viewModel.lastName
    }

}
