//
//  UserInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol UserBusinessLogic {
    func banUser(userId: Int)
    func changeUserPassword(userId: Int)
    func getUsers()
    func addUserToProject(userId: Int, projectId: Int) 
}

protocol UserDataStore {
    var options: [UserDataStoreOptionsKey : Any] { get set }
}

enum UserDataStoreOptionsKey: String {
    case some
}

class UserInteractor: UserBusinessLogic, UserDataStore {
    
    // MARK: - Properties
    
    var presenter: UserPresentationLogic?
    let worker: UserWorkerLogic
    var options: [UserDataStoreOptionsKey : Any] = [:]
    let disposeBag = DisposeBag()

    
    // MARK: - Initializer

    init(presenter: UserPresentationLogic?, worker: UserWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
    func banUser(userId: Int) {
        worker.ban(userId: userId)
        .subscribe(onCompleted: {
            self.presenter?.presentReload()
        }, onError: { error in
            print(error)
            }).disposed(by: disposeBag)
    }
    
    func changeUserPassword(userId: Int) {
        worker.changeUserPassword(userId: userId)
        .subscribe(onCompleted: {
            return
        }, onError: { error in
            print(error)
            }).disposed(by: disposeBag)
    }
    
    func getUsers() {
        worker.getUsers().subscribe(onSuccess: { data in
            self.presenter?.presentGetUser(response: .success(data))
        }, onError: { error in
            self.presenter?.presentGetUser(response: .error(error))
            }).disposed(by: disposeBag)
    }
    
    func addUserToProject(userId: Int, projectId: Int) {
        worker.addUserToProject(userId: userId, projectId: projectId).subscribe(onCompleted: {
            self.presenter?.presentAddUser(response: .success)
        }, onError: { error in
            self.presenter?.presentAddUser(response: .error(error))
        }).disposed(by: disposeBag)
    }
}
