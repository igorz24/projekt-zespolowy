//
//  UserWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol UserWorkerLogic {
    func ban(userId: Int) -> Completable
    func changeUserPassword(userId: Int) -> Completable
    func getUsers() -> Single<[UserResponseDto]>
    func addUserToProject(userId: Int, projectId: Int) -> Completable
}

class UserWorker: UserWorkerLogic {
    
    // MARK: - Properties
    
    let customerAPIService: CustomerAPIServiceProtocol
    
    // MARK: - Init
    
    init(customerAPIService: CustomerAPIServiceProtocol) {
        self.customerAPIService = customerAPIService
    }

    
    func ban(userId: Int) -> Completable {
        return customerAPIService.ban(dto: BanDto(userId: userId))
    }
    
    func changeUserPassword(userId: Int) -> Completable {
        return customerAPIService.changeUserPassword(dto: UserPasswordChangeDto(userId: userId))
    }
    
    func getUsers() -> Single<[UserResponseDto]> {
        return customerAPIService.getUsers()
    }
    
    func addUserToProject(userId: Int, projectId: Int) -> Completable {
        return customerAPIService.addUserToProject(dto: AddUserToProjectDto(userId: userId, projectId: projectId))
    }

    // MARK: - Methods
}
