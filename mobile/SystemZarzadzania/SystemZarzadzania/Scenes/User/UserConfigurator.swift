//
//  UserConfigurator.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class UserConfigurator {
    
    // MARK: - Configuration
    
    func configure(viewController: UserViewController) {
        let presenter = UserPresenter(viewController: viewController)
        let worker = UserWorker(customerAPIService: CustomerFactory.apiService())
        let interactor = UserInteractor(presenter: presenter, worker: worker)
        let router = UserRouter(viewController: viewController, dataStore: interactor)

        viewController.interactor = interactor
        viewController.router = router
    }
}
