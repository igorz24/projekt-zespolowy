//
//  UserViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

protocol UserDisplayLogic: AnyObject {
    func displayUsers(viewModel: [User.GetUsers.ViewModel])
    func showError(error: Error)
    func showReload()
}

class UserViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    var interactor: UserBusinessLogic?
    var router: (NSObjectProtocol & UserRoutingLogic & UserDataPassing)?
    var data: [User.GetUsers.ViewModel] = []
    
        
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        UserConfigurator().configure(viewController: self)
    }

    
    // MARK: - Routing
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let scene = segue.identifier {
//            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
//            if let router = router, router.responds(to: selector) {
//                router.perform(selector, with: segue)
//            }
//        }
//    }
    
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor?.getUsers()
    }
}

extension UserViewController: UserDisplayLogic {
    func showReload() {
        interactor?.getUsers()
    }
    
    
    func displayUsers(viewModel: [User.GetUsers.ViewModel]) {
        data = viewModel
        tableView.reloadData()
    }
}

extension UserViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UserTableViewCell.cellIdentifier) as? UserTableViewCell else { fatalError() }
        
        cell.setup(viewModel: data[indexPath.row])
        return cell
    }
}

extension UserViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
            let userId = data[indexPath.row].id
            let deleteAction = UIContextualAction(style: .destructive, title: "Zbanuj") { (action, view, completionHandler) in
                self.interactor?.banUser(userId: userId)
                completionHandler(true)
            }
        
            let modifyAction = UIContextualAction(style: .normal, title: "Wymuś zmiane hasła") { (action, view,  completionHandler) in
                self.interactor?.changeUserPassword(userId: userId)
                completionHandler(true)
            }
        
            let addUser = UIContextualAction(style: .normal, title: "Dodaj do projektu") { (action, view,  completionHandler) in
                self.router?.routeToAddUser(userId: userId)
                completionHandler(true)
            }

        
            let config = UISwipeActionsConfiguration(actions: [modifyAction, addUser, deleteAction])
            return config
    }
}

extension UserViewController: AddUserDelegate {
    func addUser(projectId: Int, userId: Int) {
        interactor?.addUserToProject(userId: userId, projectId: projectId)
    }
}
