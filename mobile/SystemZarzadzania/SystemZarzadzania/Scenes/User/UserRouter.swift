//
//  UserRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

@objc protocol UserRoutingLogic {
    func routeToAddUser(userId: Int)
}

protocol UserDataPassing {
    var dataStore: UserDataStore? { get }
}

class UserRouter: NSObject, UserRoutingLogic, UserDataPassing {
    
    // MARK: - Properties
    
    weak var viewController: UserViewController?
    var dataStore: UserDataStore?
    
    
    // MARK: - Initializer

    init(viewController: UserViewController?, dataStore: UserDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }

    
    // MARK: - Methods
    
    func routeToAddUser(userId: Int) {
        guard let vc = UIStoryboard.viewController(for: .addUserToProject) as? AddUserToProjectViewController else { return }
        
        vc.userId = userId
        vc.delegate = viewController
        vc.modalPresentationStyle = .overFullScreen
        vc.modalPresentationCapturesStatusBarAppearance = true
        vc.modalTransitionStyle = .coverVertical
        
        viewController?.present(vc, animated: true)
    }
    
    
    // MARK: - Navigation
    
//    func navigateToSomewhere(source: UserViewController, destination: SomewhereViewController) {
//        source.show(destination, sender: nil)
//    }
    
    
    // MARK: - Passing data
    
//    func passDataToSomewhere(source: UserDataStore, destination: inout SomewhereDataStore) {
//        destination.name = source.name
//    }
}
