//
//  UserPresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol UserPresentationLogic {
    func presentGetUser(response: User.GetUsers.Response)
    func presentAddUser(response: User.AddUser.Response)
    func presentReload()
}

class UserPresenter: UserPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: UserDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: UserDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods
    
    func presentReload() {
        viewController?.showReload()
    }
    
    func presentGetUser(response: User.GetUsers.Response) {
        switch response {
        case .success(let data):
            viewController?.displayUsers(viewModel: data.map { User.GetUsers.ViewModel(id: $0.id, email: $0.email, role: $0.role, firstName: $0.firstName, lastName: $0.lastName) })
        case .error(let error):
            viewController?.showError(error: error)
        }
    }
    
    func presentAddUser(response: User.AddUser.Response) {
        switch response {
        case .error(let error):
            viewController?.showError(error: error)
        default:
            return
        }
    }
}
