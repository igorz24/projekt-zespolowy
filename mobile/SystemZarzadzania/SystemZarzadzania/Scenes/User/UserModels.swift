//
//  UserModels.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 16/04/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum User {
    
    // MARK: - Use cases
    
    enum GetUsers {
        struct Request {}
        
        enum Response {
            case success([UserResponseDto])
            case error(Error)
        }
        
        struct ViewModel {
            let id: Int
            let email: String
            let role: String
            let firstName: String
            let lastName: String
        }
    }
    
    enum AddUser {
        enum Response {
            case success
            case error(Error)
        }
    }
}
