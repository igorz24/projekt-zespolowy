//
//  CommitViewController.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

protocol CommitDisplayLogic: AnyObject {
    func deleteSuccess(handler: (Bool) -> Void)
    func deleteError(message: String, handler: (Bool) -> Void)
    func approvalSuccess(handler: (Bool) -> Void)
    func approvalError(message: String, handler: (Bool) -> Void)
    func showError(error: Error)
    func displayGetCommits(viewModel: [Commit.GetCommits.ViewModel])
}

class CommitViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var approvalSwitch: UISwitch!
    @IBOutlet weak var approvalLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - IBActions
    
    @IBAction func approvalSwitchAction(_ sender: Any) {
        
    }
    @IBAction func filterAction(_ sender: Any) {
        interactor?.getCommits(request: Commit.GetCommits.Request(searchString: searchTextField.text, status: statusTextField.text, waiting: approvalSwitch.isOn))
    }
    
    @IBAction func AddNewCommitAction(_ sender: Any) {
        router?.routeToAddCommit()
    }
    
    // MARK: - Properties
    
    var interactor: CommitBusinessLogic?
    var router: (NSObjectProtocol & CommitRoutingLogic & CommitDataPassing)?
    var data: [Commit.GetCommits.ViewModel] = []
    var isApprovalOn: Bool = false
    
        
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        CommitConfigurator().configure(viewController: self)
    }

    
    
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100.0
        let role = UserDefaultsHelper.getRole()
        
        if role == .user {
            approvalSwitch.isHidden = true
            approvalLabel.isHidden = true
        }
        
        interactor?.getCommits(request: Commit.GetCommits.Request(searchString: nil, status: nil, waiting: approvalSwitch.isOn))
    }
}

extension CommitViewController: CommitDisplayLogic {
    func deleteSuccess(handler: (Bool) -> Void) {
        handler(true)
    }
    
    func deleteError(message: String, handler: (Bool) -> Void) {
        handler(false)
        showError(error: AppError(message: message))
    }
    
    func displayGetCommits(viewModel: [Commit.GetCommits.ViewModel]) {
        data = viewModel
        tableView.reloadData()
    }
    
    func approvalSuccess(handler: (Bool) -> Void) {
        handler(true)
    }
    
    func approvalError(message: String, handler: (Bool) -> Void) {
        handler(false)
        showError(error: AppError(message: message))
    }
}

extension CommitViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommitTableViewCell.cellIdentifier) as? CommitTableViewCell else {
            fatalError()
        }
        
        cell.setup(data: data[indexPath.row])
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    
}

extension CommitViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let role = UserDefaultsHelper.getRole()
        
        let commitId = data[indexPath.row].commitId
        let deleteAction = UIContextualAction(style: .destructive, title: "Usuń") { (action, view, completionHandler) in
            self.interactor?.deleteCommit(request: Commit.Delete.Request(completionHandler: completionHandler, commitId: commitId))
        }
        
        let modifyAction = UIContextualAction(style: .normal, title: "Modyfikuj") { (action, view, completionHandler) in
            self.router?.routeToModifyCommit(commitId: commitId)
            completionHandler(true)
        }
        
        if approvalSwitch.isOn {
            let approveAction = UIContextualAction(style: .normal, title: "Zatwierdź") { (action, view, completionHandler) in
                self.interactor?.approveCommit(request: Commit.ApproveCommit.Request(completionHandler: completionHandler, commitId: commitId))
            }
            
            let config = UISwipeActionsConfiguration(actions: [approveAction])
            return config
        } else if role == .admin {
            return nil
        }
        
        let config = UISwipeActionsConfiguration(actions: [modifyAction, deleteAction])
        return config
    }
}
