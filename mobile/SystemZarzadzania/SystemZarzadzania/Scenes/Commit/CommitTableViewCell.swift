//
//  CommitTableVTableViewCell.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright © 2020 Szymon Miketa. All rights reserved.
//

import UIKit

class CommitTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var workTimeLabel: UILabel!
    @IBOutlet weak var projectLabel: UILabel!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static let cellIdentifier = "CommitTableViewCell"

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(data: Commit.GetCommits.ViewModel) {
        dateLabel.text = data.date
        workTimeLabel.text = data.workTime
        projectLabel.text = data.project
        taskLabel.text = data.task
        commentLabel.text = data.comment
        statusLabel.text = data.status
    }

}
