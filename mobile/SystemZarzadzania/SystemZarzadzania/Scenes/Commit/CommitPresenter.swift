//
//  CommitPresenter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

protocol CommitPresentationLogic {
    func presentGetCommits(response: Commit.GetCommits.Response)
    func presentDeleteCommit(response: Commit.Delete.Response)
    func presentApproveCommit(response: Commit.ApproveCommit.Response)
}

class CommitPresenter: CommitPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: CommitDisplayLogic?
    
    
    // MARK: - Initializers

    init(viewController: CommitDisplayLogic?) {
        self.viewController = viewController
    }
    
    
    // MARK: - Methods
    
    func presentDeleteCommit(response: Commit.Delete.Response) {
        switch response {
        case .success(let handler):
            viewController?.deleteSuccess(handler: handler)
        case .error(let error, let handler):
            viewController?.deleteError(message: error.localizedDescription, handler: handler)
            
        }
    }
    
    func presentGetCommits(response: Commit.GetCommits.Response) {
        switch response {
        case .success(let data):
            let viewModel = data.map { Commit.GetCommits.ViewModel(date: CoreDateFormatter.dateAndTimeStringLocale(from: $0.date) ?? "Brak daty", workTime: String($0.workTime), project: String($0.projectId), task: String($0.taskId), comment: $0.description, status: $0.status, commitId: $0._id) }
           viewController?.displayGetCommits(viewModel: viewModel)
        case .error(let error):
            viewController?.showError(error: error)
        }
    }
    
    func presentApproveCommit(response: Commit.ApproveCommit.Response) {
        switch response {
        case .success(let handler):
            viewController?.approvalSuccess(handler: handler)
        case .error(let error, let handler):
            viewController?.approvalError(message: error.localizedDescription, handler: handler)
        }
    }
}
