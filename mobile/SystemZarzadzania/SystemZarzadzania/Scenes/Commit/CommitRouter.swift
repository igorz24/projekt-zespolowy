//
//  CommitRouter.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import UIKit

@objc protocol CommitRoutingLogic {
    func routeToAddCommit()
    func routeToModifyCommit(commitId: Int)
}

protocol CommitDataPassing {
    var dataStore: CommitDataStore? { get }
}

class CommitRouter: NSObject, CommitRoutingLogic, CommitDataPassing {
    
    // MARK: - Properties
    
    weak var viewController: CommitViewController?
    var dataStore: CommitDataStore?
    
    
    // MARK: - Initializer

    init(viewController: CommitViewController?, dataStore: CommitDataStore?) {
        self.viewController = viewController
        self.dataStore = dataStore
    }

    
    // MARK: - Methods
    
    func routeToAddCommit() {
        viewController?.navigationController?.pushViewController(UIStoryboard.viewController(for: .addCommit), animated: true)
    }
    
    func routeToModifyCommit(commitId: Int) {
        guard let vc = UIStoryboard.viewController(for: .addCommit) as? AddCommitViewController , var store = vc.router?.dataStore else { return }
        vc.purpose = .modify
        passData(commitId: commitId, dataStore: &store)
        
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func passData(commitId: Int, dataStore: inout AddCommitDataStore) {
        dataStore.commitId = commitId
    }
    
}
