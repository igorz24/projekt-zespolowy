//
//  CommitModels.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

enum Commit {
    
    // MARK: - Use cases
    
    enum GetCommits {
        struct Request {
            let searchString: String?
            let status: String?
            let waiting: Bool
        }
        
        enum Response {
            case success([CommitResponseDto])
            case error(Error)
        }
        
        struct ViewModel {
            let date: String
            let workTime: String
            let project: String
            let task: String
            let comment: String
            let status: String
            let commitId: Int
        }
    }
    
    enum Delete {
        struct Request {
            let completionHandler: (Bool) -> Void
            let commitId: Int
        }
        
        enum Response {
            case success((Bool) -> Void)
            case error(Error, (Bool) -> Void)
        }
    }
    
    enum ApproveCommit {
        struct Request {
            let completionHandler: (Bool) -> Void
            let commitId: Int
        }
        
        enum Response {
            case success((Bool) -> Void)
            case error(Error, (Bool) -> Void)
        }
    }
}
