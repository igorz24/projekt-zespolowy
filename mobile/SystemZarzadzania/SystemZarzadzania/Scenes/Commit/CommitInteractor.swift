//
//  CommitInteractor.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol CommitBusinessLogic {
    func getCommits(request: Commit.GetCommits.Request)
    func deleteCommit(request: Commit.Delete.Request)
    func approveCommit(request: Commit.ApproveCommit.Request)
}

protocol CommitDataStore {
    var options: [CommitDataStoreOptionsKey : Any] { get set }
}

enum CommitDataStoreOptionsKey: String {
    case some
}

class CommitInteractor: CommitBusinessLogic, CommitDataStore {
    
    // MARK: - Properties
    
    var presenter: CommitPresentationLogic?
    let worker: CommitWorkerLogic
    var options: [CommitDataStoreOptionsKey : Any] = [:]
    var disposeBag = DisposeBag()

    
    // MARK: - Initializer

    init(presenter: CommitPresentationLogic?, worker: CommitWorkerLogic) {
        self.presenter = presenter
        self.worker = worker
    }

    
    // MARK: - Methods
    
    func getCommits(request: Commit.GetCommits.Request) {
        worker.getCommits(request: request)
        .subscribe(onSuccess: { data in
            self.presenter?.presentGetCommits(response: .success(data))
        }, onError: { error in
            self.presenter?.presentGetCommits(response: .error(error))
        })
        .disposed(by: disposeBag)
    }
    
    func deleteCommit(request: Commit.Delete.Request) {
        worker.deleteCommit(commitId: request.commitId)
        .subscribe(onCompleted: {
            self.presenter?.presentDeleteCommit(response: .success(request.completionHandler))
        }, onError: { error in
            self.presenter?.presentDeleteCommit(response: .error(error, request.completionHandler))
        })
        .disposed(by: disposeBag)
    }
    
    func approveCommit(request: Commit.ApproveCommit.Request) {
        worker.approveCommit(commitId: request.commitId)
            .subscribe(onCompleted: {
                self.presenter?.presentApproveCommit(response: .success(request.completionHandler))
            }, onError: { error in
                self.presenter?.presentApproveCommit(response: .error(error, request.completionHandler))
            })
            .disposed(by: disposeBag)
    }
}
