//
//  CommitWorker.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation
import RxSwift

protocol CommitWorkerLogic {
    func deleteCommit(commitId: Int) -> Completable
    func getCommits(request: Commit.GetCommits.Request) -> Single<[CommitResponseDto]>
    func approveCommit(commitId: Int) -> Completable
}

class CommitWorker: CommitWorkerLogic {
    
    let customerAPIService: CustomerAPIServiceProtocol
    
    // MARK: - Init
    
    init(customerAPIService: CustomerAPIServiceProtocol) {
        self.customerAPIService = customerAPIService
    }
    

    // MARK: - Methods
    
    func deleteCommit(commitId: Int) -> Completable {
        return customerAPIService.deleteCommit(dto: DeleteCommitRequestDto(commitId: commitId))
    }
    
    func getCommits(request: Commit.GetCommits.Request) -> Single<[CommitResponseDto]> {
        if !request.waiting {
             let role = UserDefaultsHelper.getRole()
            if role == .admin {
                return customerAPIService.getAllCommits(dto: CommitRequestDto(status: request.status, textSearch: request.searchString))
            } else {
                return customerAPIService.getCommit(dto: CommitRequestDto(status: request.status, textSearch: request.searchString))
            }
        } else {
            return customerAPIService.getCommitWaiting()
        }
    }
    
    func approveCommit(commitId: Int) -> Completable {
        return customerAPIService.approveCommit(dto: ApproveCommitRequestDto(commitId: commitId))
    }
}
