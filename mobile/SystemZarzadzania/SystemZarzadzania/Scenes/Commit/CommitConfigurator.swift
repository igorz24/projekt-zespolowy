//
//  CommitConfigurator.swift
//  SystemZarzadzania
//
//  Created by Szymon Miketa on 17/03/2020.
//  Copyright (c) 2020 Szymon Miketa. All rights reserved.
//

import Foundation

class CommitConfigurator {
    
    // MARK: - Configuration
    
    func configure(viewController: CommitViewController) {
        let presenter = CommitPresenter(viewController: viewController)
        let worker = CommitWorker(customerAPIService: CustomerFactory.apiService())
        let interactor = CommitInteractor(presenter: presenter, worker: worker)
        let router = CommitRouter(viewController: viewController, dataStore: interactor)

        viewController.interactor = interactor
        viewController.router = router
    }
}
