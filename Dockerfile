ARG IMAGE=node:10
FROM $IMAGE as builder

WORKDIR /app
# Install app dependencies
COPY . .

RUN npm i -g typescript
RUN npm install
RUN tsc

COPY env.production /app/build

EXPOSE 4000
# Run server
ENV NODE_ENV dev
CMD ["node", "/app/build/index.js"]